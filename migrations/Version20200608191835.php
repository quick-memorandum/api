<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608191835 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Make all tables have an owner';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE items ADD user_id UUID NOT NULL');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94DA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E11EE94DA76ED395 ON items (user_id)');
        $this->addSql('ALTER INDEX idx_1f1b251e111948dc RENAME TO IDX_E11EE94D111948DC');
        $this->addSql('ALTER TABLE grains ADD user_id UUID NOT NULL');
        $this->addSql('ALTER TABLE grains ADD CONSTRAINT FK_D1CF2B88A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D1CF2B88A76ED395 ON grains (user_id)');
        $this->addSql('ALTER INDEX idx_7b2609f1126f525e RENAME TO IDX_D1CF2B88126F525E');
        $this->addSql('ALTER INDEX idx_7b2609f1f39ebe7a RENAME TO IDX_D1CF2B88F39EBE7A');
        $this->addSql('ALTER TABLE decks ADD user_id UUID NOT NULL');
        $this->addSql('ALTER TABLE decks ADD CONSTRAINT FK_A3FCC632A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A3FCC632A76ED395 ON decks (user_id)');
        $this->addSql('ALTER TABLE batches ADD user_id UUID NOT NULL');
        $this->addSql('ALTER TABLE batches ADD CONSTRAINT FK_F06E6553A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F06E6553A76ED395 ON batches (user_id)');
        $this->addSql('ALTER INDEX idx_f80b52d4111948dc RENAME TO IDX_F06E6553111948DC');
        $this->addSql('ALTER INDEX uniq_8d93d649e7927c74 RENAME TO UNIQ_1483A5E9E7927C74');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE batches DROP CONSTRAINT FK_F06E6553A76ED395');
        $this->addSql('DROP INDEX IDX_F06E6553A76ED395');
        $this->addSql('ALTER TABLE batches DROP user_id');
        $this->addSql('ALTER INDEX idx_f06e6553111948dc RENAME TO idx_f80b52d4111948dc');
        $this->addSql('ALTER TABLE decks DROP CONSTRAINT FK_A3FCC632A76ED395');
        $this->addSql('DROP INDEX IDX_A3FCC632A76ED395');
        $this->addSql('ALTER TABLE decks DROP user_id');
        $this->addSql('ALTER TABLE grains DROP CONSTRAINT FK_D1CF2B88A76ED395');
        $this->addSql('DROP INDEX IDX_D1CF2B88A76ED395');
        $this->addSql('ALTER TABLE grains DROP user_id');
        $this->addSql('ALTER INDEX idx_d1cf2b88f39ebe7a RENAME TO idx_7b2609f1f39ebe7a');
        $this->addSql('ALTER INDEX idx_d1cf2b88126f525e RENAME TO idx_7b2609f1126f525e');
        $this->addSql('ALTER TABLE items DROP CONSTRAINT FK_E11EE94DA76ED395');
        $this->addSql('DROP INDEX IDX_E11EE94DA76ED395');
        $this->addSql('ALTER TABLE items DROP user_id');
        $this->addSql('ALTER INDEX idx_e11ee94d111948dc RENAME TO idx_1f1b251e111948dc');
        $this->addSql('ALTER INDEX uniq_1483a5e9e7927c74 RENAME TO uniq_8d93d649e7927c74');
    }
}
