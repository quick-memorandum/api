<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200525213802 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Make all the tables plural';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE batch RENAME TO batches');
        $this->addSql('ALTER TABLE deck RENAME TO decks');
        $this->addSql('ALTER TABLE grain RENAME TO grains');
        $this->addSql('ALTER TABLE item RENAME TO items');
        $this->addSql('ALTER TABLE "user" RENAME TO users');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE batches RENAME TO batch');
        $this->addSql('ALTER TABLE decks RENAME TO deck');
        $this->addSql('ALTER TABLE grains RENAME TO grain');
        $this->addSql('ALTER TABLE items RENAME TO item');
        $this->addSql('ALTER TABLE users RENAME TO "user"');
    }
}
