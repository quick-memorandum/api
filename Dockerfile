# the different stages of this Dockerfile are meant to be built into separate images
# https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage
# https://docs.docker.com/compose/compose-file/#target


# https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG PHP_VERSION=8.0
ARG NGINX_VERSION=1.17
ARG VARNISH_VERSION=6.4


# "php" stage
FROM php:${PHP_VERSION}-fpm-alpine AS api_platform_php

# persistent / runtime deps
RUN apk add --no-cache \
		acl \
		fcgi \
		file \
		gettext \
		git \
	;

# FIXME Hardcoded dev environment.
# Can easily be switched to "prod" for a production build
# then xdebug and composer dev requirements won't be installed.
ARG APP_ENV=dev

ARG APCU_VERSION=5.1.19
ARG XDEBUG_VERSION=3.0.3
RUN set -eux; \
	apk add --no-cache --virtual .build-deps \
		$PHPIZE_DEPS \
		icu-dev \
		libzip-dev \
		postgresql-dev \
		zlib-dev \
        rabbitmq-c-dev \
	; \
	\
	docker-php-ext-configure zip; \
	docker-php-ext-install -j$(nproc) \
		intl \
		pdo_pgsql \
		zip \
	; \
	pecl install \
		apcu-${APCU_VERSION} \
	; \
	pecl clear-cache; \
	docker-php-ext-enable \
		apcu \
		opcache \
	;

# build and install amqp with PHP 8 support
# TODO: Replace with pecl package after a new release (see https://github.com/php-amqp/php-amqp/issues/386#issuecomment-795115965).
RUN docker-php-source extract; \
	mkdir /usr/src/php/ext/amqp; \
	curl -L https://github.com/php-amqp/php-amqp/archive/v1.11.0beta.tar.gz | tar -xzC /usr/src/php/ext/amqp --strip-components=1; \
	docker-php-ext-install amqp

RUN if [ "$APP_ENV" == "dev" ]; then \
    pecl install \
    	xdebug-${XDEBUG_VERSION} \
    ; \
    pecl clear-cache; \
    docker-php-ext-enable \
		xdebug \
	; \
fi

RUN runDeps="$( \
	scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
		| tr ',' '\n' \
		| sort -u \
		| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --no-cache --virtual .api-phpexts-rundeps $runDeps; \
	apk del .build-deps

COPY --from=composer:2.0 /usr/bin/composer /usr/bin/composer

RUN ln -s $PHP_INI_DIR/php.ini-`if [ "$APP_ENV" == "dev" ]; then echo 'development'; else echo 'production'; fi` $PHP_INI_DIR/php.ini
COPY docker/php/conf.d/api-platform.$APP_ENV.ini $PHP_INI_DIR/conf.d/api-platform.ini

RUN set -eux; \
	{ \
		echo '[www]'; \
		echo 'ping.path = /ping'; \
	} | tee /usr/local/etc/php-fpm.d/docker-healthcheck.conf

ENV SYMFONY_PHPUNIT_VERSION=9

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
# install Symfony Flex globally to speed up download of Composer packages (parallelized prefetching)
RUN set -eux; \
	composer global require "symfony/flex" --prefer-dist --no-progress --no-suggest --classmap-authoritative; \
	composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"

WORKDIR /srv/api

# prevent the reinstallation of vendors at every changes in the source code
COPY composer.json composer.lock symfony.lock ./
RUN set -eux; \
	composer install --prefer-dist --no-scripts --no-progress --no-suggest `if [ "$APP_ENV" != "dev" ]; then echo '--no-dev'; fi`; \
	composer clear-cache

# do not use .env files
COPY .env ./
RUN composer dump-env $APP_ENV; \
	rm .env

# copy only specifically what we need
COPY bin bin/
COPY config config/
COPY public public/
COPY src src/

RUN set -eux; \
	mkdir -p var/cache var/log; \
	composer dump-autoload --classmap-authoritative `if [ "$APP_ENV" != "dev" ]; then echo '--no-dev'; fi`; \
	composer run-script post-install-cmd `if [ "$APP_ENV" != "dev" ]; then echo '--no-dev'; fi`; \
	chmod +x bin/console; sync
VOLUME /srv/api/var

COPY docker/php/docker-healthcheck.sh /usr/local/bin/docker-healthcheck
RUN chmod +x /usr/local/bin/docker-healthcheck

HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD ["docker-healthcheck"]

COPY docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]


# "nginx" stage
# depends on the "php" stage above
FROM nginx:${NGINX_VERSION}-alpine AS api_platform_nginx

COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

WORKDIR /srv/api/public

COPY --from=api_platform_php /srv/api/public ./


# "varnish" stage
# does not depend on any of the above stages, but placed here to keep everything in one Dockerfile
FROM varnish:${VARNISH_VERSION} AS api_platform_varnish

COPY docker/varnish/conf/default.vcl /etc/varnish/default.vcl

CMD ["varnishd", "-F", "-f", "/etc/varnish/default.vcl", "-p", "http_resp_hdr_len=65536", "-p", "http_resp_size=98304"]
