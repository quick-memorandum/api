<?php

namespace App\Tests\Entity;

use App\Account\Domain\User\Model\User;
use App\Tests\Api\ApiTestCase;
use App\Tests\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

final class UserTest extends ApiTestCase
{
    use FixturesTrait;

    protected function setUp(): void
    {
        $this->loadFixtures([
            UserFixtures::class,
        ]);
    }

    protected function getResourceName(): string
    {
        return User::class;
    }

    public function testCreateUser()
    {
        $resource = $this->postResource([
            'email' => 'user@example.com',
            'password' => 'passwd123',
        ], '/users', null);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'email',
                'username' => 'user@example.com',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
        $this->assertArrayNotHasKey('roles', $responseArray);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCreateUserWithRoles()
    {
        $resource = $this->postResource([
            'email' => 'user@example.com',
            'password' => 'passwd123',
            'roles' => [
                User::ROLE_USER,
                User::ROLE_ADMIN,
            ],
        ], '/users', null);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'email',
                'username' => 'user@example.com',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
        $this->assertArrayNotHasKey('roles', $responseArray);

        // ensure field "roles" was ignored
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => $responseArray['id'] ?? null]);
        $this->getResource($iri, $token);

        $this->assertJsonContains([
            'roles' => [
                User::ROLE_USER,
            ],
        ]);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCreateUserWithRolesByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'email' => 'user@example.com',
            'password' => 'passwd123',
            'roles' => [
                User::ROLE_USER,
                User::ROLE_ADMIN,
            ],
        ], '/users', $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'email',
                'username' => 'user@example.com',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
        $this->assertArrayNotHasKey('roles', $responseArray);

        // ensure field "roles" was ignored
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => $responseArray['id'] ?? null]);
        $this->getResource($iri, $token);

        $this->assertJsonContains([
            'roles' => [
                User::ROLE_USER,
            ],
        ]);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCreateUserWithRolesByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);

        $resource = $this->postResource([
            'email' => 'user@example.com',
            'password' => 'passwd123',
            'roles' => [
                User::ROLE_ADMIN,
                User::ROLE_USER,
            ],
        ], '/users', $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'email',
                'username' => 'user@example.com',
                'roles',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);

        // ensure field "roles" was used
        $iri = $this->findIriBy(User::class, ['id' => $responseArray['id'] ?? null]);
        $this->getResource($iri, $token);

        $this->assertJsonContains([
            'roles' => [
                User::ROLE_ADMIN,
                User::ROLE_USER,
            ],
        ]);
    }

    public function testCreateUserAndLogin()
    {
        $this->postResource([
            'email' => 'user@example.com',
            'password' => 'passwd123',
        ], '/users', null);

        $resource = $this->postResource([
            'username' => 'user@example.com',
            'password' => 'passwd123',
        ], '/authentication_token', null);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResponseStatusCodeSame(200);
        $this->assertArrayHasKey('token', $responseArray);
        $this->assertArrayHasKey('refresh_token', $responseArray);
    }

    public function testCreateBlankUser()
    {
        $resource = $this->postResource([
            'email' => '',
            'password' => '',
        ], '/users', null);

        $this->assertResourceBasicValidationTest($resource, [
            'email',
            'password',
            'password',
        ]);
    }

    public function testCreateTooLongUser()
    {
        $resource = $this->postResource([
            'email' => 'DxtwYWAA2Wf2gZFaocx16z5dkNmPlTr9cfrBq5JcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKz@example.com',
            'password' => 'pO2WjG93PIKa6UyiLQOpeVdOCrymU1tj5LogI3IrzhTIrBJ3hE0QOgsjzeTUGtMa6AxAte8NulGqpJz5tihkUCSsOwHSYOJ3kuv5heQdcgqM6mPTLPLwZUJoms1qRl8dwRgoRHileJI061ZOiDxUvmHfcPGijwE6lvHpmVjh06O1eYyflwNxpyjPkbXCN6EFIlLeRKUAKQdNiCdJwHLhzQOb2CLE0hAwY67Kxpk3gfXMlEgtY9zXAmABQko5mwIg',
        ], '/users', null);

        $this->assertResourceBasicValidationTest($resource, [
            'email',
            'password',
        ]);
    }

    public function testCreateUserWithInvalidEmail()
    {
        $resource = $this->postResource([
            'email' => 'abcqwertyxyz',
            'password' => 'password',
        ], '/users', null);

        $this->assertResourceBasicValidationTest($resource, [
            'email',
        ]);
    }

    public function testCreateUserWithTooShortPassword()
    {
        $resource = $this->postResource([
            'email' => 'user@example.com',
            'password' => 'pO2WjG9',
        ], '/users', null);

        $this->assertResourceBasicValidationTest($resource, [
            'password',
        ]);
    }

    public function testGetAuthorizedUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->getResource($iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $resource->setResourceFields([
            'email' => 'main@example.com',
            'username' => 'main@example.com',
        ]);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            'cf50cfad-cd66-3ec5-b945-5670efa51634',
            [
                'email',
                'username',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
        $this->assertArrayNotHasKey('roles', $responseArray);
    }

    public function testGetAuthorizedAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => 'd3f2433f-57cc-3fc5-948f-63674de31b2b']);

        $resource = $this->getResource($iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $resource->setResourceFields([
            'email' => 'admin@example.com',
            'username' => 'admin@example.com',
            'roles' => [
                User::ROLE_ADMIN,
                User::ROLE_USER,
            ],
        ]);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            'd3f2433f-57cc-3fc5-948f-63674de31b2b',
            [
                'email',
                'username',
                'roles',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testGetOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $resource = $this->getResource($iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $resource->setResourceFields([
            'email' => 'additional@example.com',
            'username' => 'additional@example.com',
        ]);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
            [
                'email',
                'username',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testGetOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $resource = $this->getResource($iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $resource->setResourceFields([
            'email' => 'additional@example.com',
            'username' => 'additional@example.com',
            'roles' => [
                User::ROLE_USER,
            ],
        ]);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
            [
                'email',
                'username',
                'roles',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testGetUserWithoutAuth()
    {
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $this->getResource($iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testPutUpdateUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->putResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], $iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            'cf50cfad-cd66-3ec5-b945-5670efa51634',
            [
                'email',
                'username' => 'updateduser@example.com',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testPatchUpdateUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->patchResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], $iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            'cf50cfad-cd66-3ec5-b945-5670efa51634',
            [
                'email',
                'username' => 'updateduser@example.com',
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testPutUpdateUserAndLogin()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $this->putResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], $iri, $token);

        // with old token
        $this->postResource([
            'username' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], '/authentication_token', $token);

        $this->assertResponseStatusCodeSame(401);

        // with new token
        $token = $this->createUser('updateduser@example.com');

        $this->postResource([
            'username' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], '/authentication_token', $token);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testPatchUpdateUserAndLogin()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $this->patchResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], $iri, $token);

        // with old token
        $this->postResource([
            'username' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], '/authentication_token', $token);

        $this->assertResponseStatusCodeSame(401);

        // with new token
        $token = $this->createUser('updateduser@example.com');

        $this->postResource([
            'username' => 'updateduser@example.com',
            'password' => 'updatedpassword',
        ], '/authentication_token', $token);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateBlankUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->putResource([
            'email' => '',
            'password' => '',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'email',
                'password',
                'password',
            ]
        );
    }

    public function testPatchUpdateBlankUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->putResource([
            'email' => '',
            'password' => '',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'email',
                'password',
                'password',
            ]
        );
    }

    public function testPutUpdateTooLongUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->putResource([
            'email' => 'DxtwYWAA2Wf2gZFaocx16z5dkNmPlTr9cfrBq5JcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKz@example.com',
            'password' => 'pO2WjG93PIKa6UyiLQOpeVdOCrymU1tj5LogI3IrzhTIrBJ3hE0QOgsjzeTUGtMa6AxAte8NulGqpJz5tihkUCSsOwHSYOJ3kuv5heQdcgqM6mPTLPLwZUJoms1qRl8dwRgoRHileJI061ZOiDxUvmHfcPGijwE6lvHpmVjh06O1eYyflwNxpyjPkbXCN6EFIlLeRKUAKQdNiCdJwHLhzQOb2CLE0hAwY67Kxpk3gfXMlEgtY9zXAmABQko5mwIg',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'email',
                'password',
            ]
        );
    }

    public function testPatchUpdateTooLongUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->patchResource([
            'email' => 'DxtwYWAA2Wf2gZFaocx16z5dkNmPlTr9cfrBq5JcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKnjOS7HJcqEfzbAXCdafwcfQ4jkdvhj1LsKz@example.com',
            'password' => 'pO2WjG93PIKa6UyiLQOpeVdOCrymU1tj5LogI3IrzhTIrBJ3hE0QOgsjzeTUGtMa6AxAte8NulGqpJz5tihkUCSsOwHSYOJ3kuv5heQdcgqM6mPTLPLwZUJoms1qRl8dwRgoRHileJI061ZOiDxUvmHfcPGijwE6lvHpmVjh06O1eYyflwNxpyjPkbXCN6EFIlLeRKUAKQdNiCdJwHLhzQOb2CLE0hAwY67Kxpk3gfXMlEgtY9zXAmABQko5mwIg',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'email',
                'password',
            ]
        );
    }

    public function testPutUpdateUserWithInvalidEmail()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->putResource([
            'email' => 'abcqwertyxyz',
            'password' => 'password',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'email',
            ]
        );
    }

    public function testPatchUpdateUserWithInvalidEmail()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->patchResource([
            'email' => 'abcqwertyxyz',
            'password' => 'password',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'email',
            ]
        );
    }

    public function testPutUpdateUserWithTooShortPassword()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->putResource([
            'email' => 'main@example.com',
            'password' => 'pO2WjG9',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'password',
            ]
        );
    }

    public function testPatchUpdateUserWithTooShortPassword()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->patchResource([
            'email' => 'main@example.com',
            'password' => 'pO2WjG9',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'password',
            ]
        );
    }

    public function testPutUpdateOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $this->putResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedPassword',
        ], $iri, $token);

        $this->assertResourceBasicAccessDenied();
    }

    public function testPatchUpdateOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $this->patchResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedPassword',
        ], $iri, $token);

        $this->assertResourceBasicAccessDenied();
    }

    public function testPutUpdateOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $resource = $this->putResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedPassword',
            'roles' => [
                User::ROLE_ADMIN,
            ],
        ], $iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
            [
                'email',
                'username' => 'updateduser@example.com',
                'roles' => [
                    User::ROLE_ADMIN,
                    User::ROLE_USER,
                ],
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testPatchUpdateOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $resource = $this->patchResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedPassword',
            'roles' => [
                User::ROLE_ADMIN,
            ],
        ], $iri, $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
            [
                'email',
                'username' => 'updateduser@example.com',
                'roles' => [
                    User::ROLE_ADMIN,
                    User::ROLE_USER,
                ],
            ]
        );

        $this->assertArrayNotHasKey('password', $responseArray);
    }

    public function testPutUpdateUserWithoutAuth()
    {
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $this->putResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedPassword',
        ], $iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testPatchUpdateUserWithoutAuth()
    {
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $this->patchResource([
            'email' => 'updateduser@example.com',
            'password' => 'updatedPassword',
        ], $iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testDeleteUser(): void
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->deleteResource($iri, $token);

        $this->assertResourceBasicSoftDeleteSuccessTest($resource);
    }

    public function testDeleteUserAndTryToUse()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $this->deleteResource($iri, $token);

        $this->getResource($iri, $token);
        $this->assertResourceUnauthorized();

        $this->getResource($iri, null);
        $this->assertResponseStatusCodeSame(404);

        $this->postResource([
            'username' => 'main@example.com',
            'password' => 'password',
        ], '/authentication_token', null);
        $this->assertResourceUnauthorized('Invalid credentials.');
    }

    public function testDeleteOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $this->deleteResource($iri, $token);

        $this->assertResourceBasicAccessDenied();
    }

    public function testDeleteOtherUserByAdmin(): void
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(User::class, ['id' => 'cf50cfad-cd66-3ec5-b945-5670efa51634']);

        $resource = $this->deleteResource($iri, $token);

        $this->assertResourceBasicSoftDeleteSuccessTest($resource);
    }

    public function testDeleteUserWithoutAuth()
    {
        $iri = $this->findIriBy(User::class, ['id' => '07ff00ad-fca2-3ab6-b8d8-7d2d23c16710']);

        $this->deleteResource($iri, null);

        $this->assertResourceBasicNoAuthTest();
    }
}
