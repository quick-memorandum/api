<?php

namespace App\Tests\Entity;

use App\Study\Domain\Batch\Model\Batch;
use App\Tests\Api\ApiTestCase;
use App\Tests\DataFixtures\BatchFixtures;
use App\Tests\DataFixtures\DeckFixtures;
use App\Tests\DataFixtures\GrainFixtures;
use App\Tests\DataFixtures\ItemFixtures;
use App\Tests\DataFixtures\UserFixtures;
use Doctrine\ORM\EntityManager;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class BatchTest extends ApiTestCase
{
    use FixturesTrait;

    protected function setUp(): void
    {
        $this->loadFixtures([
            DeckFixtures::class,
            ItemFixtures::class,
            BatchFixtures::class,
            GrainFixtures::class,
            UserFixtures::class,
        ]);
    }

    protected function getResourceName(): string
    {
        return Batch::class;
    }

    public function testCreateBatch()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => 'name',
            'deck' => '/decks/56feafc3-75be-3450-89eb-5d4d82ad1828',
        ], '/batches', $token, [], false);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $this->assertEmpty($responseArray);
        $this->assertResponseStatusCodeSame($resource->getExpectedStatusCode());

        /** @var EntityManager $em */
        $em = self::$container->get('doctrine.orm.default_entity_manager');
        /** @var Batch $batch */
        $batch = $em->getRepository(Batch::class)->findOneBy(['name' => 'name'], ['createdAt' => 'desc']);

        // Ensure "grains" were generated properly
        $resource = $this->getResource(
            $this->findIriBy(Batch::class, ['id' => $batch->getId()]).'?replaceProperties[deck][]=id&replaceProperties[grains][]=id',
            $token
        );
        $response = $resource->getResponse();
        $responseArray = $response->getArray();
        $this->assertEquals('/decks/56feafc3-75be-3450-89eb-5d4d82ad1828', $responseArray['deck']['@id']);
        $this->assertCount(3, $responseArray['grains']);
    }

    public function testCreateBatchWithBadDeck()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);

        $resource = $this->postResource([
            'name' => 'name',
            'deck' => '/decks/140be9fc-c457-35b6-8187-2bbc25f27f8e',
            'user' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634',
        ], '/batches', $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'deck' => 'This resource belongs to the "main@example.com". The "additional@example.com" passed.',
            ]
        );
    }

    public function testCreateBlankBatch()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => '',
            'deck' => '/decks/56feafc3-75be-3450-89eb-5d4d82ad1828',
        ], '/batches', $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'name',
                'name',
            ]
        );
    }

    public function testCreateTooLongBatch()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => '6BDowQu4NXt2gxIy5qJqmMR0GvBeHY0d8BPdQowbZbFw4QENX9x9qXVxxUvYfGY9S4HJxF4WXgeWQ1liPNougApkAlTNwgGDKgihYuwYTwD6MHBwSPjA9wvqDcRzgaGuTmOcbR74kJtLuopowA4IEc0aChAx0zeN3sZ9hg3D9RVURsVR2W0ezfKQCV1aZfxC4zEcHc5kSaj0Mu6X97MbjGRV7f1LMrbYJ2aT6Z4XgbfSmzaDm1QSGPVsXNgElQQl',
            'deck' => '/decks/56feafc3-75be-3450-89eb-5d4d82ad1828',
        ], '/batches', $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'name',
            ]
        );
    }
}
