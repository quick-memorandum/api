<?php

namespace App\Tests\Entity;

use App\Creator\Domain\Deck\Model\Deck;
use App\Tests\Api\ApiTestCase;
use App\Tests\DataFixtures\DeckFixtures;
use App\Tests\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class DeckTest extends ApiTestCase
{
    use FixturesTrait;

    protected function setUp(): void
    {
        $this->loadFixtures([
            DeckFixtures::class,
            UserFixtures::class,
        ]);
    }

    protected function getResourceName(): string
    {
        return Deck::class;
    }

    public function testCreateDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => 'name',
            'description' => 'desc',
        ], '/decks', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testCreateDeckWithItems()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => 'name',
            'description' => 'desc',
            'items' => [
                [
                    'front' => 'abc',
                    'back' => 'def',
                ],
                [
                    'front' => 'ghi',
                    'back' => 'jkl',
                ],
            ],
        ], '/decks?appendProperties[]=items', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
                'items' => [
                    [
                        '@type' => 'Item',
                        'front' => 'abc',
                        'back' => 'def',
                    ],
                ],
            ]
        );
    }

    public function testCreateDeckWithInvalidItems()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => 'name',
            'description' => 'desc',
            'items' => [
                [
                    'front' => 'abc',
                    'back' => '',
                ],
                [
                    'front' => '',
                    'back' => '',
                ],
            ],
        ], '/decks?appendProperties[]=items', $token);

        $this->assertResourceBasicValidationTest($resource, [
            'items[0].back',
            'items[0].back',
            'items[1].front',
            'items[1].front',
            'items[1].back',
            'items[1].back',
        ]);
    }

    public function testCreateDeckWithOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => 'name',
            'description' => 'desc',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], '/decks', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testCreateDeckWithOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);

        $resource = $this->postResource([
            'name' => 'name',
            'description' => 'desc',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], '/decks', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'name',
                'description',
                'user' => ['@id' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710'],
            ]
        );
    }

    public function testCreateBlankNameDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => '',
        ], '/decks', $token);

        $this->assertResourceBasicValidationTest($resource, [
            'name',
            'name',
        ]);
    }

    public function testCreateTooLongNameDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'name' => 'TXcKucwQV9xuUUO6lpZFti2jfwkocUkQ89zCCXrAZjkqMDZGmmI4sh5S9hpNgSr91mLeSXOBV0kKDk6JD93eGHxJTCBU4wUGh9MLXFFDADXnF0csJW7fAhRaoY28mqsaW0mEvaAjAsBetId0r8qzSSWDo4po7B6qD8VIQPKHaPJtAHTPinAAJMWKIT4MkB1DEkeAJ27HZ8ufwfnkyxryJUVNKVJUp9J87FCtCKu1y7yycVz0gL7bJybhZcTL14NT',
            'description' => 'desc',
        ], '/decks', $token);

        $this->assertResourceBasicValidationTest($resource, [
            'name',
        ]);
    }

    public function testCreateDeckWithoutAuth()
    {
        $this->postResource([
            'name' => 'name',
            'description' => 'desc',
        ], '/decks', null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testGetDeckCollection()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->getResource('/decks', $token);

        $this->assertResourceBasicGetCollectionSuccessTest(
            $resource,
            DeckFixtures::SIZE_PER_USER
        );
    }

    public function testGetDeckCollectionWithoutAuth()
    {
        $this->getResource('/decks', null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testGetDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->getResource($iri, $token);

        $resource->setResourceFields([
            'name' => 'Humberto Yoshikoville',
            'description' => 'Id in nihil distinctio in temporibus rerum aut. Rerum totam est rerum et. Officia laborum est possimus eos iste excepturi et. Repellendus beatae molestiae quis voluptatem.',
            'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
        ]);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user',
            ]
        );
    }

    public function testGetDeckWithoutAuth()
    {
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);
        $this->getResource($iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testGetSomebodyDeck()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $adminUserToken = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->getResource($iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->getResource($iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(200);

        $this->getResource($iri, $adminUserToken);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testPatchUpdateDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testPutUpdateBlankNameDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->putResource([
            'name' => '',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'name',
            'name',
        ]);
    }

    public function testPatchUpdateBlankNameDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->patchResource([
            'name' => '',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'name',
            'name',
        ]);
    }

    public function testPutUpdateTooLongNameDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->putResource([
            'name' => 'TXcKucwQV9xuUUO6lpZFti2jfwkocUkQ89zCCXrAZjkqMDZGmmI4sh5S9hpNgSr91mLeSXOBV0kKDk6JD93eGHxJTCBU4wUGh9MLXFFDADXnF0csJW7fAhRaoY28mqsaW0mEvaAjAsBetId0r8qzSSWDo4po7B6qD8VIQPKHaPJtAHTPinAAJMWKIT4MkB1DEkeAJ27HZ8ufwfnkyxryJUVNKVJUp9J87FCtCKu1y7yycVz0gL7bJybhZcTL14NT',
            'description' => 'desc',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'name',
        ]);
    }

    public function testPatchUpdateTooLongNameDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->patchResource([
            'name' => 'TXcKucwQV9xuUUO6lpZFti2jfwkocUkQ89zCCXrAZjkqMDZGmmI4sh5S9hpNgSr91mLeSXOBV0kKDk6JD93eGHxJTCBU4wUGh9MLXFFDADXnF0csJW7fAhRaoY28mqsaW0mEvaAjAsBetId0r8qzSSWDo4po7B6qD8VIQPKHaPJtAHTPinAAJMWKIT4MkB1DEkeAJ27HZ8ufwfnkyxryJUVNKVJUp9J87FCtCKu1y7yycVz0gL7bJybhZcTL14NT',
            'description' => 'desc',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'name',
        ]);
    }

    public function testPutUpdateDeckWithoutAuth()
    {
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testPatchUpdateDeckWithoutAuth()
    {
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testPutUpdateSomebodyDeckByUser()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPatchUpdateSomebodyDeckUser()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateSomebodyDeckByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $token);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPatchUpdateSomebodyDeckByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
        ], $iri, $token);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateDeckWithOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testPatchUpdateDeckWithOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testPutUpdateDeckWithOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->putResource([
            'name' => 'new name',
            'description' => 'new desc',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user' => ['@id' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710'],
            ]
        );
    }

    public function testPatchUpdateDeckWithOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->patchResource([
            'name' => 'new name',
            'description' => 'new desc',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '027f3399-4060-3d0c-a1d9-ffbe91b42925',
            [
                'name',
                'description',
                'user' => ['@id' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710'],
            ]
        );
    }

    public function testDeleteDeck()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $resource = $this->deleteResource($iri, $token);

        $this->assertResourceBasicDeleteSuccessTest($resource);
    }

    public function testDeleteDeckWithoutAuth()
    {
        $iri = $this->findIriBy(Deck::class, ['id' => '027f3399-4060-3d0c-a1d9-ffbe91b42925']);

        $this->deleteResource($iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testDeleteSomebodyDeckByUser()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->deleteResource($iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->deleteResource($iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(204);
    }

    public function testDeleteSomebodyDeckByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Deck::class, ['id' => '140be9fc-c457-35b6-8187-2bbc25f27f8e']);

        $this->deleteResource($iri, $token);
        $this->assertResponseStatusCodeSame(204);
    }
}
