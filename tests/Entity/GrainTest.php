<?php

namespace App\Tests\Entity;

use App\Study\Domain\Grain\Model\Grain;
use App\Tests\Api\ApiTestCase;
use App\Tests\DataFixtures\BatchFixtures;
use App\Tests\DataFixtures\DeckFixtures;
use App\Tests\DataFixtures\GrainFixtures;
use App\Tests\DataFixtures\ItemFixtures;
use App\Tests\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class GrainTest extends ApiTestCase
{
    use FixturesTrait;

    protected function setUp(): void
    {
        $this->loadFixtures([
            DeckFixtures::class,
            ItemFixtures::class,
            BatchFixtures::class,
            GrainFixtures::class,
            UserFixtures::class,
        ]);
    }

    protected function getResourceName(): string
    {
        return Grain::class;
    }

    public function testRandGrain()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = '/grains/837d6d9b-2be9-3abc-a87c-c9977ad51425/rand';

        $resource = $this->getResource($iri.'?appendProperties[]=item', $token);
        $response = $resource->getResponse();
        $responseArray = $response->getArray();

        $resource->setResourceFields([
            'level' => 5,
            'finished' => false,
            'batch' => ['@id' => '/batches/837d6d9b-2be9-3abc-a87c-c9977ad51425'],
            'item' => [
                '@type' => 'Item',
            ],
        ]);

        $this->assertArrayHasKey('id', $resource->getResponse()->getArray()['item']);
        $this->assertArrayHasKey('front', $resource->getResponse()->getArray()['item']);
        $this->assertArrayHasKey('back', $resource->getResponse()->getArray()['item']);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'level',
                'finished',
                'batch',
                'item',
            ]
        );

        $this->assertArrayHasKey('@id', $responseArray['item']);
        $this->assertArrayHasKey('id', $responseArray['item']);
        $this->assertArrayHasKey('front', $responseArray['item']);
        $this->assertArrayHasKey('back', $responseArray['item']);
    }

    public function testRandGrainNotFoundGrain()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = '/grains/47d6426e-1d54-3270-b43a-c8590c7278d9/rand';

        $resource = $this->getResource($iri, $token);

        $this->assertResponseStatusCodeSame(404);
        $this->assertResponseHeaderSame('content-type', $resource->getExpectedContentType());

        $this->assertJsonContains([
            '@context' => '/contexts/Error',
            '@type' => 'hydra:Error',
            'hydra:title' => 'An error occurred',
            'hydra:description' => 'The Grain Not Found',
        ]);
    }

    public function testRandGrainNotFoundBatch()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = '/grains/5aa3aca8-8bf8-11ea-bc55-0242ac130003/rand';

        $resource = $this->getResource($iri, $token);

        $this->assertResponseStatusCodeSame(404);
        $this->assertResponseHeaderSame('content-type', $resource->getExpectedContentType());

        $this->assertJsonContains([
            '@context' => '/contexts/Error',
            '@type' => 'hydra:Error',
            'hydra:title' => 'An error occurred',
            'hydra:description' => 'The Batch Not Found',
        ]);
    }
}
