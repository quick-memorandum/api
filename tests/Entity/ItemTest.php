<?php

namespace App\Tests\Entity;

use App\Creator\Domain\Flashcard\Model\Item;
use App\Tests\Api\ApiTestCase;
use App\Tests\DataFixtures\DeckFixtures;
use App\Tests\DataFixtures\ItemFixtures;
use App\Tests\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class ItemTest extends ApiTestCase
{
    use FixturesTrait;

    protected function setUp(): void
    {
        $this->loadFixtures([
            DeckFixtures::class,
            ItemFixtures::class,
            UserFixtures::class,
        ]);
    }

    protected function getResourceName(): string
    {
        return Item::class;
    }

    public function testCreateItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'deck' => '/decks/027f3399-4060-3d0c-a1d9-ffbe91b42925',
        ], '/items', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'front',
                'back',
                'deck' => ['@id' => '/decks/027f3399-4060-3d0c-a1d9-ffbe91b42925'],
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testCreateItemWithOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'deck' => '/decks/027f3399-4060-3d0c-a1d9-ffbe91b42925',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], '/items', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'front',
                'back',
                'deck' => ['@id' => '/decks/027f3399-4060-3d0c-a1d9-ffbe91b42925'],
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testCreateItemWithOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);

        $resource = $this->postResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'deck' => '/decks/140be9fc-c457-35b6-8187-2bbc25f27f8e',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], '/items', $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            null,
            [
                'front',
                'back',
                'deck' => ['@id' => '/decks/140be9fc-c457-35b6-8187-2bbc25f27f8e'],
                'user' => ['@id' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710'],
            ]
        );
    }

    public function testCreateItemWithBadDeck()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);

        $resource = $this->postResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'deck' => '/decks/140be9fc-c457-35b6-8187-2bbc25f27f8e',
            'user' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634',
        ], '/items', $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'deck' => 'This resource belongs to the "main@example.com". The "additional@example.com" passed.',
            ]
        );
    }

    public function testCreateBlankItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'front' => '',
            'back' => '',
        ], '/items', $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'front',
                'front',
                'back',
                'back',
            ]
        );
    }

    public function testCreateTooLongItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->postResource([
            'front' => '1yYZlaEuzVbpNgOKF0QowSNuCklXfgsh9Vu7OzJtsJGeKAqrXOcoL8y1t8u9bXaIaf1OWQUmaLoaW0A5jtvQBFxVmNau8IVad03Ls08N92U31NquuZbC9Cy2OJr4PLk2iz3AjtS7hkPQPOklCaEqiRIaFgkcxc8eNC8DVfOF048EgkyhjHEtBsccGmjem7bk3bAI6iCFtR5rLM4J89c3X6bzX8fQy8CafK4hDftvx7EukFWFOSJxTa1pOXtsuwhr',
            'back' => 'LgXfGVYcsMmM5oanIAvqMwhUJNUfQCogjmgVGeP1hv1nqUVIGU8BQknJZVLJbZfvYd7gF5RNZtqoFiHwR1u8ilIiEB5oOpIFsDqLv56LzcFwj7nhyqUuRHS8Itnb0XuG2BfnEqiuWI6gAZJszoVnykPU7CBCnj39Y1etaoOYXgOFfpngMPOly1tKnvUarHTmxoG2zQUGWZhVleFMCaEmPAvaxFDx9W8twvYDO2IwKlFDNv8gAjBI97UsxoA7UmLa',
        ], '/items', $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'front',
                'back',
            ]
        );
    }

    public function testCreateItemWithoutAuth()
    {
        $this->postResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'deck' => '/decks/027f3399-4060-3d0c-a1d9-ffbe91b42925',
        ], '/items', null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testGetItemCollection()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);

        $resource = $this->getResource('/items', $token);

        $this->assertResourceBasicGetCollectionSuccessTest(
            $resource,
            ItemFixtures::SIZE_PER_USER
        );
    }

    public function testGetItemCollectionWithoutAuth()
    {
        $this->getResource('/items', null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testGetItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->getResource($iri, $token);

        $resource->setResourceFields([
            'front' => 'Fugit sunt nihil ut.',
            'back' => 'Consequatur laborum est qui.',
            'deck' => '/decks/97348ef6-1a03-3991-9a78-284b1fbc0987',
        ]);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '009f843d-f05b-3018-ba08-7cdfc89ff1c4',
            [
                'front',
                'back',
                'deck' => ['@id' => '/decks/97348ef6-1a03-3991-9a78-284b1fbc0987'],
            ]
        );
    }

    public function testGetItemWithoutAuth()
    {
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $this->getResource($iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testGetSomebodyItem()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $adminUserToken = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->getResource($iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->getResource($iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(200);

        $this->getResource($iri, $adminUserToken);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->putResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '009f843d-f05b-3018-ba08-7cdfc89ff1c4',
            [
                'front',
                'back',
                'deck' => ['@id' => '/decks/97348ef6-1a03-3991-9a78-284b1fbc0987'],
            ]
        );
    }

    public function testPatchUpdateItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->patchResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '009f843d-f05b-3018-ba08-7cdfc89ff1c4',
            [
                'front',
                'back',
                'deck' => ['@id' => '/decks/97348ef6-1a03-3991-9a78-284b1fbc0987'],
            ]
        );
    }

    public function testPutUpdateBlankItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->putResource([
            'front' => '',
            'back' => '',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'front',
            'front',
            'back',
            'back',
        ]);
    }

    public function testPatchUpdateBlankItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->patchResource([
            'front' => '',
            'back' => '',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'front',
            'front',
            'back',
            'back',
        ]);
    }

    public function testPutUpdateTooLongItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->putResource([
            'front' => '1yYZlaEuzVbpNgOKF0QowSNuCklXfgsh9Vu7OzJtsJGeKAqrXOcoL8y1t8u9bXaIaf1OWQUmaLoaW0A5jtvQBFxVmNau8IVad03Ls08N92U31NquuZbC9Cy2OJr4PLk2iz3AjtS7hkPQPOklCaEqiRIaFgkcxc8eNC8DVfOF048EgkyhjHEtBsccGmjem7bk3bAI6iCFtR5rLM4J89c3X6bzX8fQy8CafK4hDftvx7EukFWFOSJxTa1pOXtsuwhr',
            'back' => 'LgXfGVYcsMmM5oanIAvqMwhUJNUfQCogjmgVGeP1hv1nqUVIGU8BQknJZVLJbZfvYd7gF5RNZtqoFiHwR1u8ilIiEB5oOpIFsDqLv56LzcFwj7nhyqUuRHS8Itnb0XuG2BfnEqiuWI6gAZJszoVnykPU7CBCnj39Y1etaoOYXgOFfpngMPOly1tKnvUarHTmxoG2zQUGWZhVleFMCaEmPAvaxFDx9W8twvYDO2IwKlFDNv8gAjBI97UsxoA7UmLa',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'front',
            'back',
        ]);
    }

    public function testPatchUpdateTooLongItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->patchResource([
            'front' => '1yYZlaEuzVbpNgOKF0QowSNuCklXfgsh9Vu7OzJtsJGeKAqrXOcoL8y1t8u9bXaIaf1OWQUmaLoaW0A5jtvQBFxVmNau8IVad03Ls08N92U31NquuZbC9Cy2OJr4PLk2iz3AjtS7hkPQPOklCaEqiRIaFgkcxc8eNC8DVfOF048EgkyhjHEtBsccGmjem7bk3bAI6iCFtR5rLM4J89c3X6bzX8fQy8CafK4hDftvx7EukFWFOSJxTa1pOXtsuwhr',
            'back' => 'LgXfGVYcsMmM5oanIAvqMwhUJNUfQCogjmgVGeP1hv1nqUVIGU8BQknJZVLJbZfvYd7gF5RNZtqoFiHwR1u8ilIiEB5oOpIFsDqLv56LzcFwj7nhyqUuRHS8Itnb0XuG2BfnEqiuWI6gAZJszoVnykPU7CBCnj39Y1etaoOYXgOFfpngMPOly1tKnvUarHTmxoG2zQUGWZhVleFMCaEmPAvaxFDx9W8twvYDO2IwKlFDNv8gAjBI97UsxoA7UmLa',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest($resource, [
            'front',
            'back',
        ]);
    }

    public function testPutUpdateItemWithoutAuth()
    {
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $this->putResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testPatchUpdateItemWithoutAuth()
    {
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $this->patchResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testPutUpdateSomebodyItemByUser()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->putResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->putResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPatchUpdateSomebodyItemByUser()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->patchResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->patchResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateSomebodyItemByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->putResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $token);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPatchUpdateSomebodyItemByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->patchResource([
            'front' => 'new flashcard',
            'back' => 'nowa fiszka',
        ], $iri, $token);
        $this->assertResponseStatusCodeSame(200);
    }

    public function testPutUpdateItemWithOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->putResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '009f843d-f05b-3018-ba08-7cdfc89ff1c4',
            [
                'front',
                'back',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testPatchUpdateItemWithOtherUserByUser()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->patchResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicGetItemSuccessTest(
            $resource,
            '009f843d-f05b-3018-ba08-7cdfc89ff1c4',
            [
                'front',
                'back',
                'user' => ['@id' => '/users/cf50cfad-cd66-3ec5-b945-5670efa51634'],
            ]
        );
    }

    public function testPutItemWithOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->putResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'deck' => 'This resource belongs to the "additional@example.com". The "main@example.com" passed.',
            ]
        );
    }

    public function testPatchItemWithOtherUserByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->patchResource([
            'front' => 'flashcard',
            'back' => 'fiszka',
            'user' => '/users/07ff00ad-fca2-3ab6-b8d8-7d2d23c16710',
        ], $iri, $token);

        $this->assertResourceBasicValidationTest(
            $resource,
            [
                'deck' => 'This resource belongs to the "additional@example.com". The "main@example.com" passed.',
            ]
        );
    }

    public function testDeleteItem()
    {
        $token = $this->createUser(UserFixtures::USER_MAIN);
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $resource = $this->deleteResource($iri, $token);

        $this->assertResourceBasicDeleteSuccessTest($resource);
    }

    public function testDeleteItemWithoutAuth()
    {
        $iri = $this->findIriBy(Item::class, ['id' => '009f843d-f05b-3018-ba08-7cdfc89ff1c4']);

        $this->deleteResource($iri, null);

        $this->assertResourceBasicNoAuthTest();
    }

    public function testDeleteSomebodyItemByUser()
    {
        $mainUserToken = $this->createUser(UserFixtures::USER_MAIN);
        $additionalUserToken = $this->createUser(UserFixtures::USER_ADDITIONAL);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->deleteResource($iri, $mainUserToken);
        $this->assertResponseStatusCodeSame(404);

        $this->deleteResource($iri, $additionalUserToken);
        $this->assertResponseStatusCodeSame(204);
    }

    public function testDeleteSomebodyItemByAdmin()
    {
        $token = $this->createUser(UserFixtures::USER_ADMIN);
        $iri = $this->findIriBy(Item::class, ['id' => '0a178bf7-609b-3e0a-a42d-1cf823898815']);

        $this->deleteResource($iri, $token);
        $this->assertResponseStatusCodeSame(204);
    }
}
