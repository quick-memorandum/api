<?php

namespace App\Tests\Filter;

use App\Account\Domain\User\UserDefinition;
use App\Creator\Domain\Deck\DeckDefinition;
use App\Shared\Application\ApiDefinition\DefinitionBuilder;
use App\Shared\UI\Filter\PropertyDefinitionFilter;
use App\Study\Domain\Batch\BatchDefinition;
use App\Study\Domain\Batch\Model\Batch;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * Tests dependent on AbstractEntityDefinition classes.
 */
class PropertyDefinitionFilterTest extends WebTestCase
{
    public function testAppendProperties()
    {
        $this->createClient();

        $request = new Request([
            PropertyDefinitionFilter::APPEND_PROPERTIES_PARAMETER => [
                'deck',
                'grains' => [
                    'id',
                    'user',
                ],
            ],
        ]);

        /** @var EntityManagerInterface $em */
        $em = self::$container->get('doctrine.orm.entity_manager');
        $context = [
            AbstractNormalizer::ATTRIBUTES => BatchDefinition::DEFAULT_PROPERTIES,
            AbstractNormalizer::GROUPS => [Batch::READ_GROUP, 'admin:read'],
        ];

        $propertyFilter = new PropertyDefinitionFilter(
            BatchDefinition::class,
            new DefinitionBuilder($em)
        );
        $propertyFilter->apply($request, true, [], $context);

        $expected = [
            'attributes' => [
                'id',
                'name',
                'level',
                'createdAt',
                'updatedAt',
                'user' => [],
                'deck' => DeckDefinition::DEFAULT_PROPERTIES,
                'grains' => [
                    'id',
                    'user' => UserDefinition::DEFAULT_PROPERTIES,
                ],
            ],
            'groups' => [
                'batch:read',
                'admin:read',
                'batch:read:additional',
                'deck:read',
                'deck:read:additional',
                'grain:read',
                'grain:read:additional',
                'user:read',
                'user:read:additional',
            ],
        ];

        $this->assertSame($expected['attributes'], $expected['attributes']);
        // don't care about the order
        $this->assertSame(array_diff($expected['groups'], $context['groups']), array_diff($context['groups'], $expected['groups']));
    }

    public function testReplaceProperties()
    {
        $this->createClient();

        $request = new Request([
            PropertyDefinitionFilter::REPLACE_PROPERTIES_PARAMETER => [
                'name',
                'deck',
                'grains' => [
                    'id',
                    'user',
                ],
            ],
        ]);

        /** @var EntityManagerInterface $em */
        $em = self::$container->get('doctrine.orm.entity_manager');
        $context = [
            AbstractNormalizer::ATTRIBUTES => BatchDefinition::DEFAULT_PROPERTIES,
            AbstractNormalizer::GROUPS => [Batch::READ_GROUP, 'admin:read'],
        ];

        $propertyFilter = new PropertyDefinitionFilter(
            BatchDefinition::class,
            new DefinitionBuilder($em)
        );
        $propertyFilter->apply($request, true, [], $context);

        $expected = [
            'attributes' => [
                'name',
                'deck' => DeckDefinition::DEFAULT_PROPERTIES,
                'grains' => [
                    'id',
                    'user' => UserDefinition::DEFAULT_PROPERTIES,
                ],
            ],
            'groups' => [
                'batch:read',
                'admin:read',
                'batch:read:additional',
                'deck:read',
                'deck:read:additional',
                'grain:read',
                'grain:read:additional',
                'user:read',
                'user:read:additional',
            ],
        ];

        $this->assertSame($expected['attributes'], $expected['attributes']);
        // don't care about the order
        $this->assertSame(array_diff($expected['groups'], $context['groups']), array_diff($context['groups'], $expected['groups']));
    }
}
