<?php

namespace App\Study\Infrastructure\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\DenormalizedIdentifiersAwareItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Creator\Domain\Deck\Model\Deck;
use App\Shared\Application\Service\SoftDeletable;
use App\Study\Domain\Batch\Model\Batch;

class BatchAlwaysWithDeckDataProvider implements ContextAwareCollectionDataProviderInterface, DenormalizedIdentifiersAwareItemDataProviderInterface, RestrictedDataProviderInterface
{
    private const COLLECTION_OPERATIONS = ['get', 'post'];
    private const ITEM_OPERATIONS = ['get', 'progress', 'delete', 'put', 'patch', 'lvlup'];

    private CollectionDataProviderInterface $collectionDataProvider;
    private ItemDataProviderInterface $itemDataProvider;
    private SoftDeletable $softDeletable;

    public function __construct(
        CollectionDataProviderInterface $collectionDataProvider,
        ItemDataProviderInterface $itemDataProvider,
        SoftDeletable $softDeletable
    ) {
        $this->collectionDataProvider = $collectionDataProvider;
        $this->itemDataProvider = $itemDataProvider;
        $this->softDeletable = $softDeletable;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        if (Batch::class !== $resourceClass) {
            return false;
        }

        if ('item' === $context['operation_type'] && in_array($operationName, self::COLLECTION_OPERATIONS)) {
            return true;
        }

        return in_array($operationName, self::ITEM_OPERATIONS);
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $this->softDeletable->disableFilter(Deck::class);

        return $this->collectionDataProvider->getCollection($resourceClass, $operationName, $context);
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        $this->softDeletable->disableFilter(Deck::class);

        return $this->itemDataProvider->getItem($resourceClass, $id, $operationName, $context);
    }
}
