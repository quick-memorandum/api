<?php

namespace App\Study\Infrastructure\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\DenormalizedIdentifiersAwareItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Creator\Domain\Flashcard\Model\Item;
use App\Shared\Application\Service\SoftDeletable;
use App\Study\Domain\Grain\Model\Grain;

class GrainAlwaysWithItemDataProvider implements ContextAwareCollectionDataProviderInterface, DenormalizedIdentifiersAwareItemDataProviderInterface, RestrictedDataProviderInterface
{
    private const COLLECTION_OPERATIONS = ['get'];
    private const ITEM_OPERATIONS = ['get', 'finish', 'lvlup'];

    private CollectionDataProviderInterface $collectionDataProvider;
    private ItemDataProviderInterface $itemDataProvider;
    private SoftDeletable $softDeletable;

    public function __construct(
        CollectionDataProviderInterface $collectionDataProvider,
        ItemDataProviderInterface $itemDataProvider,
        SoftDeletable $softDeletable
    ) {
        $this->collectionDataProvider = $collectionDataProvider;
        $this->itemDataProvider = $itemDataProvider;
        $this->softDeletable = $softDeletable;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        if (Grain::class !== $resourceClass) {
            return false;
        }

        if ('item' === $context['operation_type'] && in_array($operationName, self::COLLECTION_OPERATIONS)) {
            return true;
        }

        return in_array($operationName, self::ITEM_OPERATIONS);
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $this->softDeletable->disableFilter(Item::class);

        return $this->collectionDataProvider->getCollection($resourceClass, $operationName, $context);
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        $this->softDeletable->disableFilter(Item::class);

        return $this->itemDataProvider->getItem($resourceClass, $id, $operationName, $context);
    }
}
