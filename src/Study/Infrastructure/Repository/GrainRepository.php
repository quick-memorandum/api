<?php

namespace App\Study\Infrastructure\Repository;

use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Grain\Model\Grain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Grain|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grain|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grain[]    findAll()
 * @method Grain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Grain::class);
    }

    /**
     * @return int|mixed|string
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function randOneActive(Batch $batch)
    {
        return $this->createQueryBuilder('grain')
            ->where('grain.batch = :batch')
            ->andWhere('grain.level = :level')
            ->andWhere('grain.finished = false')
            ->setParameters([
                'batch' => $batch,
                'level' => $batch->getLevel(),
            ])
            ->orderBy('RAND()')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return array
     */
    public function countFinishedGroupedByLevel(Batch $batch)
    {
        return $this->createQueryBuilder('grain')
            ->select('COUNT(grain.id) as finished, grain.level')
            ->where('grain.batch = :batch')
            ->andWhere('grain.finished = true')
            ->groupBy('grain.level')
            ->setParameter('batch', $batch)
            ->getQuery()
            ->getScalarResult();
    }

    public function existsUnprocessedInBatch(Batch $batch): bool
    {
        try {
            return null !== $this->createQueryBuilder('t')
                ->select('1')
                ->where('t.batch = :batch')
                ->andWhere('t.finished = false')
                ->andWhere('t.level = :currentLevel OR t.level = :nextLevel')
                ->setParameters([
                    'batch' => $batch,
                    'currentLevel' => $batch->getLevel(),
                    'nextLevel' => $batch->getLevel() + 1,
                ])
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return false;
        }
    }
}
