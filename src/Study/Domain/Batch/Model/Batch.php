<?php

namespace App\Study\Domain\Batch\Model;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserDefinition;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use App\Account\Domain\User\Validator\Ownership;
use App\Creator\Domain\Deck\DeckDefinition;
use App\Creator\Domain\Deck\Model\Deck;
use App\Shared\Application\ApiDefinition\Definition;
use App\Shared\UI\Filter\PropertyDefinitionFilter;
use App\Study\Application\Command;
use App\Study\Application\Dto\Batch\BatchProgressOutput;
use App\Study\Domain\Batch\BatchDefinition;
use App\Study\Domain\Grain\GrainDefinition;
use App\Study\Domain\Grain\Model\Grain;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Set of Grains which makes an independent variant of the Deck.
 *
 * @ApiResource(
 *      attributes={
 *          "security"="is_granted('ROLE_USER')",
 *          "normalization_context"={"groups"={Batch::READ_GROUP}},
 *          "denormalization_context"={"groups"={Batch::CREATE_GROUP, Batch::UPDATE_GROUP}}
 *      },
 *      collectionOperations={
 *          "get",
 *          "post"={
 *              "status"=202,
 *              "messenger"="input",
 *              "input"=Command\Batch\CreateBatch::class,
 *              "output"=false,
 *              "denormalization_context"={"groups"={Batch::CREATE_GROUP}}
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "progress"={
 *              "method"="GET",
 *              "path"="/batches/{id}/progress",
 *              "output"=BatchProgressOutput::class
 *          },
 *          "delete",
 *          "put"={
 *              "denormalization_context"={"groups"={Batch::UPDATE_GROUP}}
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={Batch::UPDATE_GROUP}}
 *          },
 *          "lvlup"={
 *              "method"="PATCH",
 *              "path"="/batches/{id}/lvlup",
 *              "messenger"="input",
 *              "input"=Command\Batch\LevelUp::class,
 *              "denormalization_context"={"groups"={}},
 *              "openapi_context"={
 *                  "summary"="Increase the Batch level.",
 *                  "add_responses"={
 *                      "422"={
 *                          "description"="Unprocessable Entity"
 *                      }
 *                  }
 *              }
 *          }
 *      }
 * )
 * @ApiFilter(PropertyDefinitionFilter::class, arguments={
 *     "entityDefinition": BatchDefinition::class
 * })
 * @ORM\Entity(repositoryClass="App\Study\Infrastructure\Repository\BatchRepository")
 * @ORM\Table(name="batches")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(hardDelete=false)
 */
#[Definition([BatchDefinition::class])]
class Batch implements UserOwnershipEntityInterface
{
    use SoftDeleteableEntity;

    const READ_GROUP = 'batch:read';
    const READ_ADDITIONAL_GROUP = 'batch:read:additional';
    const CREATE_GROUP = 'batch:create';
    const UPDATE_GROUP = 'batch:update';

    /**
     * @var string|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid", unique=true)
     *
     * @Groups({Batch::READ_GROUP})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(allowNull=true)
     * @Assert\Length(min="3", max="255")
     *
     * @Groups({
     *     Batch::READ_GROUP,
     *     Batch::CREATE_GROUP,
     *     Batch::UPDATE_GROUP
     * })
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({Batch::READ_GROUP})
     */
    private $level = 1;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @Groups({Batch::READ_GROUP})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     * @Groups({Batch::READ_GROUP})
     */
    private $updatedAt;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({
     *     Batch::READ_GROUP,
     *     User::WRITE_ADMIN_GROUP,
     *     UserOwnershipEntityInterface::AUTO_WRITE_GROUP
     * })
     */
    #[Definition([UserDefinition::class])]
    private $user;

    /**
     * @var Deck
     *
     * @ORM\ManyToOne(targetEntity=Deck::class, inversedBy="batches")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Ownership()
     *
     * @Groups({
     *     Batch::READ_GROUP,
     *     Batch::CREATE_GROUP
     * })
     */
    #[Definition([DeckDefinition::class])]
    private $deck;

    /**
     * @var Collection|Grain[]
     *
     * @ORM\OneToMany(targetEntity=Grain::class, mappedBy="batch", cascade={"remove"})
     *
     * @Groups({Batch::READ_ADDITIONAL_GROUP})
     */
    #[Definition([GrainDefinition::class])]
    private $grains;

    public function __construct()
    {
        $this->grains = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDeck(): Deck
    {
        return $this->deck;
    }

    public function setDeck(Deck $deck): self
    {
        $this->deck = $deck;

        return $this;
    }

    public function getGrains(): Collection
    {
        return $this->grains;
    }
}
