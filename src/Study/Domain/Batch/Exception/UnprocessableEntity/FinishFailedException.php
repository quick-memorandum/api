<?php

namespace App\Study\Domain\Batch\Exception\UnprocessableEntity;

use App\Shared\Domain\Exception\UnprocessableEntityInterface;

final class FinishFailedException extends \Exception implements UnprocessableEntityInterface
{
}
