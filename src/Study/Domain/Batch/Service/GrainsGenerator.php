<?php

namespace App\Study\Domain\Batch\Service;

use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Grain\Model\Grain;
use Doctrine\ORM\EntityManagerInterface;

class GrainsGenerator
{
    private EntityManagerInterface $entityManager;
    private int $batchProcessingSize;

    public function __construct(EntityManagerInterface $entityManager, int $batchProcessingSize)
    {
        $this->entityManager = $entityManager;
        $this->batchProcessingSize = $batchProcessingSize;
    }

    public function generateFromBatch(Batch $batch)
    {
        $items = $batch->getDeck()->getItems();
        $itemsCount = $items->count();

        foreach ($items as $item) {
            $grain = new Grain();
            $grain->setUser($batch->getUser());
            $grain->setBatch($batch);
            $grain->setItem($item);
            $this->entityManager->persist($grain);

            if (0 === $itemsCount % $this->batchProcessingSize) {
                $this->entityManager->flush();
            }
        }

        $this->entityManager->flush();
    }
}
