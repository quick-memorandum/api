<?php

namespace App\Study\Domain\Batch\Service;

use App\Study\Domain\Batch\Model\Batch;
use App\Study\Infrastructure\Repository\GrainRepository;

class BatchProgressCalculator
{
    private GrainRepository $grainRepo;

    public function __construct(GrainRepository $grainRepo)
    {
        $this->grainRepo = $grainRepo;
    }

    public function getProgressDetails(Batch $batch): array
    {
        $finishedLevelsCounter = $this->grainRepo->countFinishedGroupedByLevel($batch);

        $grainsAmount = $this->grainRepo->count(['batch' => $batch]);
        $finishedGrainsAmount = array_sum(array_column($finishedLevelsCounter, 'finished'));
        $totalProgress = $this->getTotalProgress($grainsAmount, $finishedGrainsAmount);

        $progressDetails = [];

        if (count($finishedLevelsCounter) > 1) {
            $leftItems = $grainsAmount;

            foreach ($finishedLevelsCounter as $item) {
                $progressDetails[] = [
                    'progress' => round($item['finished'] / $leftItems * 100),
                    'finished' => $item['finished'],
                    'level' => $item['level'],
                ];

                $leftItems -= $item['finished'];
            }
        }

        return [
            'all_grains' => $grainsAmount,
            'total_progress' => $totalProgress,
            'progress_details' => $progressDetails,
        ];
    }

    public function getTotalProgress(int $grainsAmount, int $finishedGrainsAmount): int
    {
        if (0 !== $grainsAmount) {
            return round($finishedGrainsAmount / $grainsAmount * 100);
        }

        return 0;
    }
}
