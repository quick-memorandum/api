<?php

namespace App\Study\Domain\Batch;

use App\Shared\Application\ApiDefinition\AbstractEntityDefinition;
use App\Study\Domain\Batch\Model\Batch;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class BatchDefinition extends AbstractEntityDefinition
{
    const DEFAULT_PROPERTIES = [
        'id',
        'name',
        'level',
        'createdAt',
        'updatedAt',
        'user' => [],
        'deck' => [],
    ];

    public function getDefaultNormalizationContext(): array
    {
        return [
            AbstractNormalizer::ATTRIBUTES => self::DEFAULT_PROPERTIES,
        ];
    }

    public function getTargetEntity(): string
    {
        return Batch::class;
    }

    public function getDefaultPropertyGroup(): string
    {
        return Batch::READ_GROUP;
    }

    public function getAdditionalPropertyGroup(): string
    {
        return Batch::READ_ADDITIONAL_GROUP;
    }
}
