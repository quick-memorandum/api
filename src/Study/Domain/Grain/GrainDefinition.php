<?php

namespace App\Study\Domain\Grain;

use App\Shared\Application\ApiDefinition\AbstractEntityDefinition;
use App\Study\Domain\Grain\Model\Grain;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class GrainDefinition extends AbstractEntityDefinition
{
    const DEFAULT_PROPERTIES = [
        'id',
        'level',
        'finished',
        'createdAt',
        'updatedAt',
        'user' => [],
        'item' => [],
        'batch' => [],
    ];

    public function getDefaultNormalizationContext(): array
    {
        return [
            AbstractNormalizer::ATTRIBUTES => self::DEFAULT_PROPERTIES,
        ];
    }

    public function getTargetEntity(): string
    {
        return Grain::class;
    }

    public function getDefaultPropertyGroup(): string
    {
        return Grain::READ_GROUP;
    }

    public function getAdditionalPropertyGroup(): string
    {
        return Grain::READ_ADDITIONAL_GROUP;
    }
}
