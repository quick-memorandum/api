<?php

namespace App\Study\Domain\Grain\Model;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserDefinition;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use App\Creator\Domain\Flashcard\ItemDefinition;
use App\Creator\Domain\Flashcard\Model\Item;
use App\Shared\Application\ApiDefinition\Definition;
use App\Shared\UI\Filter\PropertyDefinitionFilter;
use App\Study\Application\Command;
use App\Study\Application\Query;
use App\Study\Domain\Batch\BatchDefinition;
use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Grain\GrainDefinition;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Accordingly prepared Item to study.
 *
 * @ApiResource(
 *      attributes={
 *          "security"="is_granted('ROLE_USER')",
 *          "normalization_context"={"groups"={Grain::READ_GROUP}},
 *      },
 *      collectionOperations={
 *          "get"
 *      },
 *      itemOperations={
 *          "get",
 *          "random"={
 *              "method"="GET",
 *              "path"="/grains/{batchId}/rand",
 *              "identifiers": {"batchId": {Batch::class, "identifier"}},
 *              "query"=Query\Grain\Rand::class,
 *              "openapi_context"={
 *                  "summary"="Retrieves random Grain resource.",
 *              }
 *          },
 *          "finish"={
 *              "method"="PATCH",
 *              "path"="/grains/{id}/finish",
 *              "messenger"="input",
 *              "input"=Command\Grain\Finish::class,
 *              "denormalization_context"={"groups"={}},
 *              "openapi_context"={
 *                  "summary"="Finish the Grain.",
 *                  "add_responses"={
 *                      "422"={
 *                          "description"="Unprocessable Entity"
 *                      }
 *                  }
 *              }
 *          },
 *          "lvlup"={
 *              "method"="PATCH",
 *              "path"="/grains/{id}/lvlup",
 *              "messenger"="input",
 *              "input"=Command\Grain\LevelUp::class,
 *              "denormalization_context"={"groups"={}},
 *              "openapi_context"={
 *                  "summary"="Increase the Grain level.",
 *                  "add_responses"={
 *                      "422"={
 *                          "description"="Unprocessable Entity"
 *                      }
 *                  }
 *              }
 *          }
 *      }
 * )
 * @ApiFilter(PropertyDefinitionFilter::class, arguments={
 *     "entityDefinition": GrainDefinition::class
 * })
 * @ORM\Entity(repositoryClass="App\Study\Infrastructure\Repository\GrainRepository")
 * @ORM\Table(name="grains")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(hardDelete=false)
 */
#[Definition([GrainDefinition::class])]
class Grain implements UserOwnershipEntityInterface
{
    use SoftDeleteableEntity;

    const READ_GROUP = 'grain:read';
    const READ_ADDITIONAL_GROUP = 'grain:read:additional';

    /**
     * @var string|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid", unique=true)
     *
     * @Groups({Grain::READ_GROUP})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({Grain::READ_GROUP})
     */
    private $level = 1;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Groups({Grain::READ_GROUP})
     */
    private $finished = false;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @Groups({Grain::READ_GROUP})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     * @Groups({Grain::READ_GROUP})
     */
    private $updatedAt;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({
     *     Grain::READ_GROUP,
     *     User::WRITE_ADMIN_GROUP,
     *     UserOwnershipEntityInterface::AUTO_WRITE_GROUP
     * })
     */
    #[Definition([UserDefinition::class])]
    private $user;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity=Item::class, inversedBy="grains", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     *
     * @Groups({Grain::READ_GROUP})
     */
    #[Definition([ItemDefinition::class])]
    private $item;

    /**
     * @var Batch
     *
     * @ORM\ManyToOne(targetEntity=Batch::class, inversedBy="grains", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     *
     * @Groups({Grain::READ_GROUP})
     */
    #[Definition([BatchDefinition::class])]
    private $batch;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getFinished(): bool
    {
        return $this->finished;
    }

    public function setFinished(bool $finished): self
    {
        $this->finished = $finished;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getItem(): Item
    {
        return $this->item;
    }

    public function setItem(Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getBatch(): Batch
    {
        return $this->batch;
    }

    public function setBatch(Batch $batch): self
    {
        $this->batch = $batch;

        return $this;
    }
}
