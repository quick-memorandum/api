<?php

namespace App\Study\Domain\Grain\Exception\UnprocessableEntity;

use App\Shared\Domain\Exception\UnprocessableEntityInterface;

final class IncreaseLevelFailedException extends \Exception implements UnprocessableEntityInterface
{
}
