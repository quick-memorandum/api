<?php

namespace App\Study\Application\QueryHandler\Grain;

use App\Creator\Domain\Flashcard\Model\Item;
use App\Shared\Application\Service\SoftDeletable;
use App\Study\Application\Query\Grain\Rand;
use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Grain\Model\Grain;
use App\Study\Infrastructure\Repository\BatchRepository;
use App\Study\Infrastructure\Repository\GrainRepository;
use Doctrine\ORM\UnexpectedResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class RandQueryHandler
{
    public function __construct(
        private SoftDeletable $softDeletable,
        private BatchRepository $batchRepo,
        private GrainRepository $grainRepo,
    ) {
    }

    public function __invoke(Rand $data)
    {
        $this->softDeletable->disableFilter(Item::class);

        /** @var Batch $batch */
        $batch = $this->batchRepo->find($data->batchId);

        if (!$batch) {
            throw new NotFoundHttpException('The Batch Not Found');
        }

        try {
            /** @var Grain $grain */
            $grain = $this->grainRepo->randOneActive($batch);
        } catch (UnexpectedResultException $e) {
            throw new NotFoundHttpException('The Grain Not Found');
        }

        return $grain;
    }
}
