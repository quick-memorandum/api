<?php

namespace App\Study\Application\CommandHandler\Batch;

use App\Account\Domain\User\Model\User;
use App\Creator\Domain\Deck\Model\Deck;
use App\Study\Application\Command\Batch\CreateBatch;
use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Batch\Service\GrainsGenerator;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateBatchCommandHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $entityManager;
    private GrainsGenerator $grainsGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        GrainsGenerator $grainsGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->grainsGenerator = $grainsGenerator;
    }

    /**
     * @throws ConnectionException
     */
    public function __invoke(CreateBatch $data)
    {
        $this->entityManager->getConnection()->beginTransaction();

        try {
            /** @var Deck $deck */
            $deck = $this->entityManager->find(Deck::class, $data->deck->getId());
            /** @var User $user */
            $user = $this->entityManager->find(User::class, $data->getUser()->getId());

            $batch = new Batch();
            $batch->setName($data->name);
            $batch->setDeck($deck);
            $batch->setUser($user);
            $this->entityManager->persist($batch);

            $this->grainsGenerator->generateFromBatch($batch);

            $this->entityManager->getConnection()->commit();
        } catch (\Exception $exception) {
            $this->entityManager->getConnection()->rollBack();
            throw $exception;
        }

        $this->entityManager->flush();
    }
}
