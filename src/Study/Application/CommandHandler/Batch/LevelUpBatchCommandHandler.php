<?php

namespace App\Study\Application\CommandHandler\Batch;

use App\Study\Application\Command\Batch\LevelUp;
use App\Study\Domain\Batch\Exception\UnprocessableEntity\IncreaseLevelFailedException;
use App\Study\Infrastructure\Repository\GrainRepository;
use Doctrine\ORM\EntityManagerInterface;

final class LevelUpBatchCommandHandler
{
    private EntityManagerInterface $entityManager;
    private GrainRepository $grainRepo;

    public function __construct(EntityManagerInterface $entityManager, GrainRepository $grainRepo)
    {
        $this->entityManager = $entityManager;
        $this->grainRepo = $grainRepo;
    }

    /**
     * @throws IncreaseLevelFailedException
     */
    public function __invoke(LevelUp $data)
    {
        $batch = $data->batch;

        if ($this->grainRepo->existsUnprocessedInBatch($batch)) {
            throw new IncreaseLevelFailedException('There are still unprocessed Grains in the Batch');
        }

        $batch->setLevel($batch->getLevel() + 1);

        $this->entityManager->persist($batch);
        $this->entityManager->flush();

        return $batch;
    }
}
