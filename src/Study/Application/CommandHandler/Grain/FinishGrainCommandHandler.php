<?php

namespace App\Study\Application\CommandHandler\Grain;

use App\Study\Application\Command\Grain\Finish;
use App\Study\Domain\Batch\Exception\UnprocessableEntity\FinishFailedException;
use Doctrine\ORM\EntityManagerInterface;

final class FinishGrainCommandHandler
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws FinishFailedException
     */
    public function __invoke(Finish $data)
    {
        $grain = $data->grain;

        if ($grain->getFinished()) {
            throw new FinishFailedException('This Grain is already finished');
        }

        if ($grain->getLevel() !== $grain->getBatch()->getLevel()) {
            throw new FinishFailedException('This Grain is on a different level than the Batch');
        }

        $grain->setFinished(true);

        $this->entityManager->persist($grain);
        $this->entityManager->flush();

        return $grain;
    }
}
