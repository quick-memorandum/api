<?php

namespace App\Study\Application\CommandHandler\Grain;

use App\Study\Application\Command\Grain\LevelUp;
use App\Study\Domain\Grain\Exception\UnprocessableEntity\IncreaseLevelFailedException;
use Doctrine\ORM\EntityManagerInterface;

final class LevelUpGrainCommandHandler
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @throws IncreaseLevelFailedException
     */
    public function __invoke(LevelUp $data)
    {
        $grain = $data->grain;

        if ($grain->getFinished()) {
            throw new IncreaseLevelFailedException('This Grain is already set as finished');
        }

        if ($grain->getLevel() !== $grain->getBatch()->getLevel()) {
            throw new IncreaseLevelFailedException('This Grain is on a different level than the Batch');
        }

        $grain->setLevel($grain->getLevel() + 1);

        $this->entityManager->persist($grain);
        $this->entityManager->flush();

        return $grain;
    }
}
