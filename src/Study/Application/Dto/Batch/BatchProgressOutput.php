<?php

namespace App\Study\Application\Dto\Batch;

use App\Study\Domain\Batch\Model\Batch;
use Symfony\Component\Serializer\Annotation\Groups;

final class BatchProgressOutput
{
    /**
     * @Groups({Batch::READ_GROUP})
     */
    public int $level;

    /**
     * @Groups({Batch::READ_GROUP})
     */
    public int $allGrains;

    /**
     * @Groups({Batch::READ_GROUP})
     */
    public int $totalProgress;

    /**
     * @Groups({Batch::READ_GROUP})
     */
    public array $progressDetails;
}
