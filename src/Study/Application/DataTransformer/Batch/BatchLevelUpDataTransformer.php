<?php

namespace App\Study\Application\DataTransformer\Batch;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Study\Application\Command\Batch\LevelUp;
use App\Study\Domain\Batch\Model\Batch;

final class BatchLevelUpDataTransformer implements DataTransformerInterface
{
    /**
     * @param LevelUp $object
     */
    public function transform($object, string $to, array $context = []): LevelUp
    {
        /** @var Batch $objectToPopulate */
        $objectToPopulate = $context['object_to_populate'];

        $object->batch = $objectToPopulate;

        return $object;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return LevelUp::class === $context['input']['class']
            && Batch::class === get_class($context['object_to_populate']);
    }
}
