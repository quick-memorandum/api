<?php

namespace App\Study\Application\DataTransformer\Batch;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Study\Application\Dto\Batch\BatchProgressOutput;
use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Batch\Service\BatchProgressCalculator;

final class BatchProgressDataTransformer implements DataTransformerInterface
{
    private BatchProgressCalculator $batchProgressCalculator;

    public function __construct(BatchProgressCalculator $batchProgressCalculator)
    {
        $this->batchProgressCalculator = $batchProgressCalculator;
    }

    /**
     * @param object&Batch $object
     * @param string $to BatchProgressOutput::class
     *
     * @return BatchProgressOutput
     */
    public function transform($object, string $to, array $context = [])
    {
        $progress = $this->batchProgressCalculator->getProgressDetails($object);

        $output = new BatchProgressOutput();
        $output->level = $object->getLevel();
        $output->allGrains = $progress['all_grains'];
        $output->totalProgress = $progress['total_progress'];
        $output->progressDetails = $progress['progress_details'];

        return $output;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return BatchProgressOutput::class === $to && $data instanceof Batch;
    }
}
