<?php

namespace App\Study\Application\DataTransformer\Grain;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Study\Application\Command\Grain\LevelUp;
use App\Study\Domain\Grain\Model\Grain;

final class GrainLevelUpDataTransformer implements DataTransformerInterface
{
    /**
     * @param LevelUp $object
     */
    public function transform($object, string $to, array $context = []): LevelUp
    {
        /** @var Grain $objectToPopulate */
        $objectToPopulate = $context['object_to_populate'];

        $object->grain = $objectToPopulate;

        return $object;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return LevelUp::class === $context['input']['class']
            && Grain::class === get_class($context['object_to_populate']);
    }
}
