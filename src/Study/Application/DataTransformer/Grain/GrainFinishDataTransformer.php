<?php

namespace App\Study\Application\DataTransformer\Grain;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Study\Application\Command\Grain\Finish;
use App\Study\Domain\Grain\Model\Grain;

final class GrainFinishDataTransformer implements DataTransformerInterface
{
    /**
     * @param Finish $object
     */
    public function transform($object, string $to, array $context = []): Finish
    {
        /** @var Grain $objectToPopulate */
        $objectToPopulate = $context['object_to_populate'];

        $object->grain = $objectToPopulate;

        return $object;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return Finish::class === $context['input']['class']
            && Grain::class === get_class($context['object_to_populate']);
    }
}
