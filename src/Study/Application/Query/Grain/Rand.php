<?php

namespace App\Study\Application\Query\Grain;

use App\Shared\Application\Query\IdentifiableQueryInterface;

final class Rand implements IdentifiableQueryInterface
{
    public string $batchId;

    public function setIdentifier(string $uuid): void
    {
        $this->batchId = $uuid;
    }
}
