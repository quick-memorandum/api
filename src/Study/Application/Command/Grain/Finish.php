<?php

namespace App\Study\Application\Command\Grain;

use App\Study\Domain\Grain\Model\Grain;

final class Finish
{
    public Grain $grain;
}
