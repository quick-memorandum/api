<?php

namespace App\Study\Application\Command\Batch;

use App\Study\Domain\Batch\Model\Batch;

final class LevelUp
{
    public Batch $batch;
}
