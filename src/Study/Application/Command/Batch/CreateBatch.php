<?php

namespace App\Study\Application\Command\Batch;

use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use App\Account\Domain\User\Validator\Ownership;
use App\Creator\Domain\Deck\Model\Deck;
use App\Study\Domain\Batch\Model\Batch;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateBatch implements UserOwnershipEntityInterface
{
    /**
     * @Assert\NotBlank(allowNull=true)
     * @Assert\Length(min="3", max="255")
     *
     * @Groups({Batch::CREATE_GROUP})
     */
    public ?string $name;

    /**
     * @Assert\NotBlank()
     * @Ownership()
     *
     * @Groups({Batch::CREATE_GROUP})
     */
    public Deck $deck;

    /**
     * @Groups({
     *     User::WRITE_ADMIN_GROUP
     * })
     */
    private ?User $user = null;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
