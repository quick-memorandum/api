<?php

namespace App\Creator\Domain\Flashcard;

use App\Creator\Domain\Flashcard\Model\Item;
use App\Shared\Application\ApiDefinition\AbstractEntityDefinition;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ItemDefinition extends AbstractEntityDefinition
{
    const DEFAULT_PROPERTIES = [
        'id',
        'front',
        'back',
        'createdAt',
        'updatedAt',
        'user' => [],
        'deck' => [],
    ];

    public function getDefaultNormalizationContext(): array
    {
        return [
            AbstractNormalizer::ATTRIBUTES => self::DEFAULT_PROPERTIES,
        ];
    }

    public function getTargetEntity(): string
    {
        return Item::class;
    }

    public function getDefaultPropertyGroup(): string
    {
        return Item::READ_GROUP;
    }

    public function getAdditionalPropertyGroup(): string
    {
        return Item::READ_ADDITIONAL_GROUP;
    }
}
