<?php

namespace App\Creator\Domain\Flashcard\Model;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserDefinition;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use App\Account\Domain\User\Validator\Ownership;
use App\Creator\Domain\Deck\DeckDefinition;
use App\Creator\Domain\Deck\Model\Deck;
use App\Creator\Domain\Flashcard\ItemDefinition;
use App\Shared\Application\ApiDefinition\Definition;
use App\Shared\UI\Filter\PropertyDefinitionFilter;
use App\Study\Domain\Grain\GrainDefinition;
use App\Study\Domain\Grain\Model\Grain;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *          "security"="is_granted('ROLE_USER')",
 *          "normalization_context"={"groups"={Item::READ_GROUP}},
 *          "denormalization_context"={"groups"={Item::CREATE_GROUP, Item::UPDATE_GROUP}}
 *      },
 *      collectionOperations={
 *          "get",
 *          "post"={
 *              "denormalization_context"={"groups"={Item::CREATE_GROUP}}
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "delete",
 *          "put"={
 *              "denormalization_context"={"groups"={Item::UPDATE_GROUP}}
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={Item::UPDATE_GROUP}}
 *          }
 *      }
 * )
 * @ApiFilter(PropertyDefinitionFilter::class, arguments={
 *     "entityDefinition": ItemDefinition::class
 * })
 * @ApiFilter(SearchFilter::class, properties={"deck": "exact"})
 * @ORM\Entity(repositoryClass="App\Creator\Infrastructure\Repository\ItemRepository")
 * @ORM\Table(name="items")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(hardDelete=false)
 */
#[Definition([ItemDefinition::class])]
class Item implements UserOwnershipEntityInterface
{
    use SoftDeleteableEntity;

    const READ_GROUP = 'item:read';
    const READ_ADDITIONAL_GROUP = 'item:read:additional';
    const CREATE_GROUP = 'item:create';
    const UPDATE_GROUP = 'item:update';

    /**
     * @var string|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid", unique=true)
     *
     * @Groups({Item::READ_GROUP})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="255")
     *
     * @Groups({
     *     Item::READ_GROUP,
     *     Item::CREATE_GROUP,
     *     Item::UPDATE_GROUP,
     *     Deck::CREATE_GROUP,
     *     Deck::UPDATE_GROUP
     * })
     */
    private $front;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="255")
     *
     * @Groups({
     *     Item::READ_GROUP,
     *     Item::CREATE_GROUP,
     *     Item::UPDATE_GROUP,
     *     Deck::CREATE_GROUP,
     *     Deck::UPDATE_GROUP
     * })
     */
    private $back;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @Groups({Item::READ_GROUP})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     * @Groups({Item::READ_GROUP})
     */
    private $updatedAt;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({
     *     Item::READ_GROUP,
     *     User::WRITE_ADMIN_GROUP,
     *     UserOwnershipEntityInterface::AUTO_WRITE_GROUP
     * })
     */
    #[Definition([UserDefinition::class])]
    private $user;

    /**
     * @var Deck
     *
     * @ORM\ManyToOne(targetEntity=Deck::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Ownership()
     *
     * @Groups({
     *     Item::READ_GROUP,
     *     Item::CREATE_GROUP
     * })
     */
    #[Definition([DeckDefinition::class])]
    private $deck;

    /**
     * @var Collection|Grain[]
     *
     * @ORM\OneToMany(targetEntity=Grain::class, mappedBy="item")
     *
     * @Groups({Item::READ_ADDITIONAL_GROUP})
     */
    #[Definition([GrainDefinition::class])]
    private $grains;

    public function __construct()
    {
        $this->grains = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFront(): ?string
    {
        return $this->front;
    }

    public function setFront(string $front): self
    {
        $this->front = $front;

        return $this;
    }

    public function getBack(): ?string
    {
        return $this->back;
    }

    public function setBack(string $back): self
    {
        $this->back = $back;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDeck(): Deck
    {
        return $this->deck;
    }

    public function setDeck(Deck $deck): void
    {
        $this->deck = $deck;
    }

    public function getGrains(): Collection
    {
        return $this->grains;
    }
}
