<?php

namespace App\Creator\Domain\Deck\Model;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserDefinition;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use App\Creator\Application\Dto\Deck\DeckSummaryOutput;
use App\Creator\Domain\Deck\DeckDefinition;
use App\Creator\Domain\Flashcard\ItemDefinition;
use App\Creator\Domain\Flashcard\Model\Item;
use App\Shared\Application\ApiDefinition\Definition;
use App\Shared\Infrastructure\Filter\SearchMultipleFilter;
use App\Shared\UI\Filter\PropertyDefinitionFilter;
use App\Study\Domain\Batch\BatchDefinition;
use App\Study\Domain\Batch\Model\Batch;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *          "security"="is_granted('ROLE_USER')",
 *          "normalization_context"={"groups"={Deck::READ_GROUP}},
 *          "denormalization_context"={"groups"={Deck::CREATE_GROUP, Deck::UPDATE_GROUP}}
 *      },
 *      collectionOperations={
 *          "get",
 *          "summary"={
 *              "method"="GET",
 *              "path"="/decks/summary",
 *              "output"=DeckSummaryOutput::class
 *          },
 *          "post"={
 *              "denormalization_context"={"groups"={Deck::CREATE_GROUP}}
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "delete",
 *          "put"={
 *              "denormalization_context"={"groups"={Deck::UPDATE_GROUP}}
 *          },
 *          "patch"={
 *              "denormalization_context"={"groups"={Deck::UPDATE_GROUP}}
 *          }
 *      }
 * )
 * @ApiFilter(PropertyDefinitionFilter::class, arguments={
 *     "entityDefinition": DeckDefinition::class
 * })
 * @ApiFilter(SearchMultipleFilter::class, properties={"name", "description"}, arguments={"parameterName": "find"})
 * @ORM\Entity(repositoryClass="App\Creator\Infrastructure\Repository\DeckRepository")
 * @ORM\Table(name="decks")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(hardDelete=false)
 */
#[Definition([DeckDefinition::class])]
class Deck implements UserOwnershipEntityInterface
{
    use SoftDeleteableEntity;

    const READ_GROUP = 'deck:read';
    const READ_ADDITIONAL_GROUP = 'deck:read:additional';
    const CREATE_GROUP = 'deck:create';
    const UPDATE_GROUP = 'deck:update';

    /**
     * @var string|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid", unique=true)
     *
     * @Groups({Deck::READ_GROUP})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="255")
     *
     * @Groups({
     *     Deck::READ_GROUP,
     *     Deck::CREATE_GROUP,
     *     Deck::UPDATE_GROUP
     * })
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({
     *     Deck::READ_GROUP,
     *     Deck::CREATE_GROUP,
     *     Deck::UPDATE_GROUP
     * })
     */
    private $description;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @Groups({Deck::READ_GROUP})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     * @Groups({Deck::READ_GROUP})
     */
    private $updatedAt;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({
     *     Deck::READ_GROUP,
     *     User::WRITE_ADMIN_GROUP,
     *     UserOwnershipEntityInterface::AUTO_WRITE_GROUP
     * })
     */
    #[Definition([UserDefinition::class])]
    private $user;

    /**
     * @var Collection|Item[]
     *
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="deck", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid()
     *
     * @Groups({
     *     Deck::READ_ADDITIONAL_GROUP,
     *     Deck::CREATE_GROUP,
     *     Deck::UPDATE_GROUP
     * })
     */
    #[Definition([ItemDefinition::class])]
    private $items;

    /**
     * @var Collection|Batch[]
     *
     * @ORM\OneToMany(targetEntity=Batch::class, mappedBy="deck")
     *
     * @Groups({Deck::READ_ADDITIONAL_GROUP})
     */
    #[Definition([BatchDefinition::class])]
    private $batches;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->batches = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setDeck($this);
            $item->setUser($this->user);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this === $item->getDeck() && $this->items->contains($item)) {
            $this->items->removeElement($item);

            $item->setDeletedAt(new \DateTime());
        }

        return $this;
    }

    public function getBatches(): Collection
    {
        return $this->batches;
    }
}
