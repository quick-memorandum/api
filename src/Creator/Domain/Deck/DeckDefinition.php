<?php

namespace App\Creator\Domain\Deck;

use App\Creator\Domain\Deck\Model\Deck;
use App\Shared\Application\ApiDefinition\AbstractEntityDefinition;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class DeckDefinition extends AbstractEntityDefinition
{
    const DEFAULT_PROPERTIES = [
        'id',
        'name',
        'description',
        'createdAt',
        'updatedAt',
        'user' => [],
    ];

    public function getDefaultNormalizationContext(): array
    {
        return [
            AbstractNormalizer::ATTRIBUTES => self::DEFAULT_PROPERTIES,
        ];
    }

    public function getTargetEntity(): string
    {
        return Deck::class;
    }

    public function getDefaultPropertyGroup(): string
    {
        return Deck::READ_GROUP;
    }

    public function getAdditionalPropertyGroup(): string
    {
        return Deck::READ_ADDITIONAL_GROUP;
    }
}
