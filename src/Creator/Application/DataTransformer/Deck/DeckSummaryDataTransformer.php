<?php

namespace App\Creator\Application\DataTransformer\Deck;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Creator\Application\Dto\Deck\DeckSummaryOutput;
use App\Creator\Domain\Deck\Model\Deck;
use App\Creator\Infrastructure\Repository\ItemRepository;

class DeckSummaryDataTransformer implements DataTransformerInterface
{
    private ItemRepository $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param object&Deck $object
     * @param string $to BatchProgressOutput::class
     *
     * @return DeckSummaryOutput
     */
    public function transform($object, string $to, array $context = [])
    {
        $output = new DeckSummaryOutput();
        $output->id = $object->getId();
        $output->name = $object->getName();
        $output->description = $object->getDescription();
        $output->itemCount = $this->itemRepository->countByDeck($object);

        return $output;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return DeckSummaryOutput::class === $to && $data instanceof Deck;
    }
}
