<?php

namespace App\Creator\Application\Dto\Deck;

use App\Creator\Domain\Deck\Model\Deck;
use Symfony\Component\Serializer\Annotation\Groups;

final class DeckSummaryOutput
{
    /**
     * @Groups({Deck::READ_GROUP})
     */
    public string $id;

    /**
     * @Groups({Deck::READ_GROUP})
     */
    public string $name;

    /**
     * @Groups({Deck::READ_GROUP})
     */
    public ?string $description;

    /**
     * @Groups({Deck::READ_GROUP})
     */
    public int $itemCount;
}
