<?php

namespace App\Creator\Infrastructure\Repository;

use App\Creator\Domain\Deck\Model\Deck;
use App\Creator\Domain\Flashcard\Model\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function countByDeck(Deck $deck): int
    {
        $queryBuilder = $this->createQueryBuilder('item');

        return $queryBuilder
            ->select($queryBuilder->expr()->count('item'))
            ->where('item.deck = :deck')
            ->setParameter('deck', $deck)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
