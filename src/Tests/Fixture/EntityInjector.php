<?php

namespace App\Tests\Fixture;

use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\Persistence\ObjectManager;
use ReflectionClass;

class EntityInjector
{
    private ObjectManager $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Assigns value to private property.
     *
     * @param mixed $value
     * @param $entity
     *
     * @return mixed|false
     */
    public function setPrivate(string $propertyName, $value, $entity)
    {
        try {
            $class = new ReflectionClass($entity);
            $property = $class->getProperty($propertyName);
            $property->setAccessible(true);
            $property->setValue($entity, $value);

            return $value;
        } catch (\ReflectionException $e) {
            return false;
        }
    }

    /**
     * Persist entity without id generation.
     *
     * @param $entity
     */
    public function persist($entity): void
    {
        $metadata = $this->manager->getClassMetadata(get_class($entity));
        $genuineIdGenerator = $metadata->idGenerator;
        $metadata->setIdGenerator(new AssignedGenerator());
        $this->manager->persist($entity);
        $metadata->setIdGenerator($genuineIdGenerator);
    }
}
