<?php

namespace App\Tests\Fixture;

use Doctrine\Persistence\ObjectManager;

trait FakeFixture
{
    /**
     * Creator of the FussyGenerator.
     */
    public function setFaker(string $name): FussyGenerator
    {
        return new FussyGenerator($name);
    }

    /**
     * Creator of the EntityInjector.
     */
    public function injector(ObjectManager $manager): EntityInjector
    {
        return new EntityInjector($manager);
    }
}
