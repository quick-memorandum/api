<?php

namespace App\Tests\Fixture;

use Faker\Factory;
use Faker\Generator;

class FussyGenerator
{
    private Generator $generator;
    private array $fussyValues;
    private int $seed;

    public function __construct(string $seed)
    {
        $this->seed = crc32($seed);
    }

    public function initFakerGenerator(): self
    {
        $faker = Factory::create();
        $faker->seed($this->seed);

        $this->generator = $faker;

        return $this;
    }

    public function initFakeValues(
        string $name,
        string $formatter,
        array $params,
        int $size,
        bool $unique = false,
        ?string $reference = null
    ): self {
        $generator = Factory::create();
        $generator->seed($reference ? crc32($reference) : $this->seed);

        for ($i = 0; $i < $size; ++$i) {
            $faker = $unique ? $generator->unique() : $generator;

            $this->fussyValues[$name][$i] = call_user_func_array([$faker, $formatter], $params);
        }

        return $this;
    }

    public function getFakerGenerator(): Generator
    {
        return $this->generator;
    }

    /**
     * @return mixed|array|object
     */
    public function getFakeValue(?string $name = null, ?int $number = null)
    {
        if (!is_null($name) && !is_null($number)) {
            return $this->fussyValues[$name][$number];
        }

        if (!is_null($name) && is_null($number)) {
            return $this->fussyValues[$name];
        }

        return (object) $this->fussyValues;
    }
}
