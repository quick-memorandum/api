<?php

namespace App\Tests\DataFixtures;

use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Grain\Model\Grain;
use App\Tests\Fixture\FakeFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class GrainFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{
    use FakeFixture;

    const OLD_NAME = 'App\DataFixtures\GrainFixtures';

    public function load(ObjectManager $manager)
    {
        $faker = $this->setFaker(self::OLD_NAME)
            ->initFakeValues('batchId', 'uuid', [], BatchFixtures::SIZE, true, BatchFixtures::OLD_NAME)
            ->initFakeValues('deckId', 'uuid', [], DeckFixtures::SIZE, true, DeckFixtures::OLD_NAME)
            ->getFakeValue();

        $levels = [
            [
                // empty
            ],
            [
                [[1], [true], 1],
                [[2], [false], 2],
            ],
            [
                [[1, 2], [true, true], 2],
                [[1, 2], [true, false], 2],
            ],
            [
                [[2, 2, 3], [true, true, true], 3],
                [[1, 3, 3], [true, true, false], 3],
            ],
            [
                [[1, 1, 1, 1, 2, 3, 3, 4, 4, 4], [true, true, true, true, true, true, false, false, false, false], 3],
                [[1, 1, 2, 2, 2, 4, 5, 5, 5, 6], [true, true, true, true, true, true, true, true, false, false], 5],
            ],
        ];

        foreach ($levels as $key => $level) {
            /** @var Batch[] $batches */
            $batches = $manager->getRepository(Batch::class)->findBy(['deck' => $faker->deckId[$key]]);

            if (!empty($level)) {
                $this->setGrains($batches, $level, $manager);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }

    private function setGrains($batches, $level, ObjectManager $manager)
    {
        /** @var Batch[] $batches */
        foreach ($batches as $batchNo => $batch) {
            $batch->setLevel($level[$batchNo][2]);

            /** @var Grain[] $grains */
            $grains = $manager->getRepository(Grain::class)->findBy(['batch' => $batch->getId()]);

            foreach ($grains as $grainNo => $grain) {
                $grain->setLevel($level[$batchNo][0][$grainNo]);
                $grain->setFinished($level[$batchNo][1][$grainNo]);

                $manager->persist($grain);
            }

            $manager->persist($batch);
        }
    }
}
