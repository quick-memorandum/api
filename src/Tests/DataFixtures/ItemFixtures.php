<?php

namespace App\Tests\DataFixtures;

use App\Account\Domain\User\Model\User;
use App\Creator\Domain\Deck\Model\Deck;
use App\Creator\Domain\Flashcard\Model\Item;
use App\Tests\Fixture\EntityInjector;
use App\Tests\Fixture\FakeFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ItemFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{
    use FakeFixture;

    const OLD_NAME = 'App\DataFixtures\ItemFixtures';

    const SIZE = 432;
    const SIZE_PER_USER = self::SIZE / 2;

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $injector = $this->injector($manager);
        $faker = $this->setFaker(self::OLD_NAME)
            ->initFakeValues('id', 'uuid', [], self::SIZE, true)
            ->initFakeValues('userId', 'uuid', [], UserFixtures::SIZE, true, UserFixtures::OLD_NAME)
            ->initFakeValues('deckId', 'uuid', [], DeckFixtures::SIZE, true, DeckFixtures::OLD_NAME)
            ->initFakeValues('front', 'text', [30], self::SIZE, false, self::OLD_NAME.':front')
            ->initFakeValues('back', 'text', [30], self::SIZE, false, self::OLD_NAME.':back')
            ->getFakeValue();

        /** @var User $mainUser */
        $mainUser = $manager->getRepository(User::class)->find($faker->userId[0]);
        /** @var User $additionalUser */
        $additionalUser = $manager->getRepository(User::class)->find($faker->userId[1]);

        /** @var Deck[] $mainUserDecks */
        $mainUserDecks = [
            0 => $manager->getRepository(Deck::class)->find($faker->deckId[0]),
            1 => $manager->getRepository(Deck::class)->find($faker->deckId[1]),
            2 => $manager->getRepository(Deck::class)->find($faker->deckId[2]),
            3 => $manager->getRepository(Deck::class)->find($faker->deckId[3]),
            10 => $manager->getRepository(Deck::class)->find($faker->deckId[4]),
            20 => $manager->getRepository(Deck::class)->find($faker->deckId[5]),
            30 => $manager->getRepository(Deck::class)->find($faker->deckId[6]),
            50 => $manager->getRepository(Deck::class)->find($faker->deckId[7]),
            100 => $manager->getRepository(Deck::class)->find($faker->deckId[8]),
        ];

        /** @var Deck[] $additionalUserDecks */
        $additionalUserDecks = [
            0 => $manager->getRepository(Deck::class)->find($faker->deckId[100]),
            1 => $manager->getRepository(Deck::class)->find($faker->deckId[101]),
            2 => $manager->getRepository(Deck::class)->find($faker->deckId[102]),
            3 => $manager->getRepository(Deck::class)->find($faker->deckId[103]),
            10 => $manager->getRepository(Deck::class)->find($faker->deckId[104]),
            20 => $manager->getRepository(Deck::class)->find($faker->deckId[105]),
            30 => $manager->getRepository(Deck::class)->find($faker->deckId[106]),
            50 => $manager->getRepository(Deck::class)->find($faker->deckId[107]),
            100 => $manager->getRepository(Deck::class)->find($faker->deckId[108]),
        ];

        $realSize = array_sum(array_keys($mainUserDecks)) + array_sum(array_keys($additionalUserDecks));

        if (self::SIZE !== $realSize) {
            throw new \Exception('SIZE constant not match the real size');
        }

        foreach ($mainUserDecks as $size => $deck) {
            $this->setItems($mainUser, $deck, $lastSize ?? 0, $size, $injector, $faker);
            $lastSize = $size + ($lastSize ?? 0);
        }

        foreach ($additionalUserDecks as $size => $deck) {
            $this->setItems($additionalUser, $deck, $lastSize ?? 0, $size, $injector, $faker);
            $lastSize = $size + ($lastSize ?? 0);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }

    private function setItems(User $user, Deck $deck, int $index, int $size, EntityInjector $injector, $faker)
    {
        for ($i = $index; $i < $size + $index; ++$i) {
            $item = new Item();

            $injector->setPrivate('id', $faker->id[$i], $item);
            $item->setUser($user);
            $item->setFront($faker->front[$i]);
            $item->setBack($faker->back[$i]);
            $item->setDeck($deck);

            $injector->persist($item);
        }
    }
}
