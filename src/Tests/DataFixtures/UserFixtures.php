<?php

namespace App\Tests\DataFixtures;

use App\Account\Domain\User\Model\User;
use App\Tests\Fixture\FakeFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{
    use FakeFixture;

    const OLD_NAME = 'App\DataFixtures\UserFixtures';

    const SIZE = 3;

    const USER_MAIN = 'main@example.com';
    const USER_ADDITIONAL = 'additional@example.com';
    const USER_ADMIN = 'admin@example.com';

    private const PASSWORD = 'password';

    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'email' => self::USER_MAIN,
                'password' => self::PASSWORD,
            ],
            [
                'email' => self::USER_ADDITIONAL,
                'password' => self::PASSWORD,
            ],
            [
                'email' => self::USER_ADMIN,
                'password' => self::PASSWORD,
                'roles' => [
                    'ROLE_ADMIN',
                ],
            ],
        ];

        $injector = $this->injector($manager);
        $faker = $this->setFaker(self::OLD_NAME)
            ->initFakeValues('id', 'uuid', [], self::SIZE, true)
            ->getFakeValue();

        for ($i = 0; $i < self::SIZE; ++$i) {
            $user = new User();
            $injector->setPrivate('id', $faker->id[$i], $user);
            $user->setEmail($users[$i]['email']);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $users[$i]['password']));
            $user->setRoles($users[$i]['roles'] ?? []);

            $injector->persist($user);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
