<?php

namespace App\Tests\DataFixtures;

use App\Account\Domain\User\Model\User;
use App\Creator\Domain\Deck\Model\Deck;
use App\Tests\Fixture\FakeFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class DeckFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{
    use FakeFixture;

    const OLD_NAME = 'App\DataFixtures\DeckFixtures';

    const SIZE = 200;
    const SIZE_PER_USER = self::SIZE / 2;

    public function load(ObjectManager $manager)
    {
        $injector = $this->injector($manager);
        $faker = $this->setFaker(self::OLD_NAME)
            ->initFakeValues('id', 'uuid', [], self::SIZE, true)
            ->initFakeValues('userId', 'uuid', [], UserFixtures::SIZE, true, 'App\DataFixtures\UserFixtures')
            ->initFakeValues('firstName', 'firstName', [], self::SIZE)
            ->initFakeValues('city', 'city', [], self::SIZE)
            ->initFakeValues('text', 'text', [], self::SIZE)
            ->getFakeValue();

        /** @var User $mainUser */
        $mainUser = $manager->getRepository(User::class)->find($faker->userId[0]);
        /** @var User $additionalUser */
        $additionalUser = $manager->getRepository(User::class)->find($faker->userId[1]);

        for ($i = 0; $i < self::SIZE; ++$i) {
            $deck = new Deck();
            $injector->setPrivate('id', $faker->id[$i], $deck);
            $deck->setUser($i < self::SIZE / 2 ? $mainUser : $additionalUser);
            $deck->setName("{$faker->firstName[$i]} {$faker->city[$i]}");
            $deck->setDescription($faker->text[$i]);

            $injector->persist($deck);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
