<?php

namespace App\Tests\DataFixtures;

use App\Account\Domain\User\Model\User;
use App\Creator\Domain\Deck\Model\Deck;
use App\Study\Domain\Batch\Model\Batch;
use App\Study\Domain\Batch\Service\GrainsGenerator;
use App\Tests\Fixture\FakeFixture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BatchFixtures extends Fixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    use FakeFixture;

    const OLD_NAME = 'App\DataFixtures\BatchFixtures';

    const SIZE = 18;

    private ?ContainerInterface $container;
    private ValidatorInterface $validator;
    private GrainsGenerator $grainsGenerator;

    public function __construct(ValidatorInterface $validator, GrainsGenerator $grainsGenerator)
    {
        $this->validator = $validator;
        $this->grainsGenerator = $grainsGenerator;
    }

    public function load(ObjectManager $manager)
    {
        $injector = $this->injector($manager);
        $faker = $this->setFaker(self::OLD_NAME)
            ->initFakeValues('id', 'uuid', [], self::SIZE, true)
            ->initFakeValues('userId', 'uuid', [], UserFixtures::SIZE, true, UserFixtures::OLD_NAME)
            ->initFakeValues('deckId', 'uuid', [], DeckFixtures::SIZE, true, DeckFixtures::OLD_NAME)
            ->initFakeValues('firstName', 'firstName', [], self::SIZE)
            ->initFakeValues('city', 'city', [], self::SIZE)
            ->getFakeValue();

        /** @var User $user */
        $user = $manager->getRepository(User::class)->find($faker->userId[0]);

        for ($i = 0; $i < self::SIZE / 2; ++$i) {
            /** @var Batch $batch */
            $batch = new Batch();
            /** @var Deck $deck */
            $deck = $manager->getRepository(Deck::class)->find($faker->deckId[$i]);
            $injector->setPrivate('id', $faker->id[$i], $batch);
            $batch->setUser($user);
            $batch->setName("{$faker->firstName[$i]} {$faker->city[$i]}");
            $batch->setDeck($deck);

            $injector->persist($batch);

            $this->grainsGenerator->generateFromBatch($batch);
        }

        for ($i = self::SIZE / 2; $i < self::SIZE; ++$i) {
            /** @var Batch $batch */
            $batch = new Batch();
            /** @var Deck $deck */
            $deck = $manager->getRepository(Deck::class)->find($faker->deckId[$i - self::SIZE / 2]);
            $injector->setPrivate('id', $faker->id[$i], $batch);
            $batch->setUser($user);
            $batch->setName("{$faker->firstName[$i]} {$faker->city[$i]}");
            $batch->setDeck($deck);

            $injector->persist($batch);

            $this->grainsGenerator->generateFromBatch($batch);
        }

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 4;
    }
}
