<?php

namespace App\Tests\Api;

/**
 * UserTrait.
 *
 * @mixin \ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase
 */
trait UserTrait
{
    public function createUser(string $username): string
    {
        if (null === static::$kernel || null === static::$kernel->getContainer()) {
            self::bootKernel();
        }

        $container = self::$kernel->getContainer();

        return $container->get('lexik_jwt_authentication.encoder')
            ->encode(['username' => $username]);
    }
}
