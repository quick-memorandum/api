<?php

namespace App\Tests\Api;

use App\Tests\Api\Crud\ResourceHandler;

/**
 * CrudTrait.
 *
 * @mixin \ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase
 */
trait CrudTrait
{
    public function postResource(array $fields, string $url, ?string $token, array $headers = [], bool $output = true): ResourceHandler
    {
        $resourceHandler = new ResourceHandler($url);
        $resourceHandler->setResourceFields($fields);
        $resourceHandler->setResourceResponseStatusCode($output ? 201 : 202);

        if ($token) {
            $headers['Authorization'] = $token;
        }

        $resourceHandler->createResponseHandler(
            $this->createClient()->request('POST', $url, [
                'json' => $fields,
                'headers' => array_unique($headers),
            ])
        );

        return $resourceHandler;
    }

    public function putResource(array $fields, string $url, ?string $token, array $headers = []): ResourceHandler
    {
        $resourceHandler = new ResourceHandler($url);
        $resourceHandler->setResourceFields($fields);
        $resourceHandler->setResourceResponseStatusCode(200);

        if ($token) {
            $headers['Authorization'] = $token;
        }

        $resourceHandler->createResponseHandler(
            $this->createClient()->request('PUT', $url, [
                'json' => $fields,
                'headers' => array_unique($headers),
            ])
        );

        return $resourceHandler;
    }

    public function patchResource(array $fields, string $url, ?string $token, array $headers = []): ResourceHandler
    {
        $resourceHandler = new ResourceHandler($url);
        $resourceHandler->setResourceFields($fields);
        $resourceHandler->setResourceResponseStatusCode(200);

        $headers['Content-Type'] = 'application/merge-patch+json';

        if ($token) {
            $headers['Authorization'] = $token;
        }

        $resourceHandler->createResponseHandler(
            $this->createClient()->request('PATCH', $url, [
                'json' => $fields,
                'headers' => array_unique($headers),
            ])
        );

        return $resourceHandler;
    }

    /**
     * @param $url
     */
    public function getResource($url, ?string $token, array $headers = []): ResourceHandler
    {
        $resourceHandler = new ResourceHandler($url);
        $resourceHandler->setResourceResponseStatusCode(200);

        if ($token) {
            $headers['Authorization'] = $token;
        }

        $resourceHandler->createResponseHandler(
            $this->createClient()->request('GET', $url, [
                'headers' => array_unique($headers),
            ])
        );

        return $resourceHandler;
    }

    /**
     * @param $url
     */
    public function deleteResource($url, ?string $token, array $headers = []): ResourceHandler
    {
        $resourceHandler = new ResourceHandler($url);
        $resourceHandler->setResourceResponseStatusCode(204);

        if ($token) {
            $headers['Authorization'] = $token;
        }

        $resourceHandler->createResponseHandler(
            $this->createClient()->request('DELETE', $url, [
                'headers' => array_unique($headers),
            ])
        );

        return $resourceHandler;
    }
}
