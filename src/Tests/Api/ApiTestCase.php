<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase as BaseApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Routing\IriConverter;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use ApiPlatform\Core\Exception\ItemNotFoundException;
use App\Tests\Api\Crud\ResourceHandler;

abstract class ApiTestCase extends BaseApiTestCase
{
    use UserTrait;
    use CrudTrait;

    public function assertResourceBasicGetItemSuccessTest(
        ResourceHandler $resourceHandler,
        ?string $id,
        array $fields
    ) {
        $response = $resourceHandler->getResponse();
        $responseArray = $response->getArray();

        $fieldNames = array_filter($fields, function ($key) {
            return is_integer($key);
        }, ARRAY_FILTER_USE_KEY);
        $fieldNamesAndValues = array_filter($fields, function ($key) {
            return is_string($key);
        }, ARRAY_FILTER_USE_KEY);

        $resourceHandler->addResourceFields(
            $fieldNamesAndValues
        );

        $fields = array_merge($fieldNames, array_keys($fieldNamesAndValues));

        if ($id) {
            $resourceHandler->addResourceFields([
                '@id' => $resourceHandler->getExpectedIri(),
                'id' => $id,
            ]);

            $fields = array_merge($fields, ['@id', 'id']);
        }

        $this->assertResponseStatusCodeSame($resourceHandler->getExpectedStatusCode());
        $this->assertResponseHeaderSame('content-type', $resourceHandler->getExpectedContentType());
        // FIXME $this->assertMatchesResourceItemJsonSchema($this->getResourceName());
        $this->assertJsonContains(
            $resourceHandler->getExpectedResponseFields(
                $this->getResourceShortName(),
                ...$fields
            )
        );
        $this->assertArrayHasKey('id', $responseArray);
        $this->assertArrayHasKey('createdAt', $responseArray);
        $this->assertArrayHasKey('updatedAt', $responseArray);
    }

    public function assertResourceBasicGetCollectionSuccessTest(
        ResourceHandler $resourceHandler,
        int $size
    ) {
        $response = $resourceHandler->getResponse();
        $responseArray = $response->getArray();
        $lastPage = ceil($size / 30);

        $this->assertResponseStatusCodeSame($resourceHandler->getExpectedStatusCode());
        $this->assertResponseHeaderSame('content-type', $resourceHandler->getExpectedContentType());

        $this->assertJsonContains([
            '@context' => "/contexts/{$this->getResourceShortName()}",
            '@id' => $resourceHandler->getExpectedIri(),
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => $size,
            'hydra:view' => [
                '@id' => $resourceHandler->getExpectedIri().'?page=1',
                '@type' => 'hydra:PartialCollectionView',
                'hydra:first' => $resourceHandler->getExpectedIri().'?page=1',
                'hydra:last' => $resourceHandler->getExpectedIri()."?page={$lastPage}",
                'hydra:next' => $resourceHandler->getExpectedIri().'?page=2',
            ],
        ]);

        $this->assertCount(30, $responseArray['hydra:member']);
        // FIXME $this->assertMatchesResourceCollectionJsonSchema("App\\Entity\\{$this->getResourceName()}");
    }

    public function assertResourceBasicDeleteSuccessTest(ResourceHandler $resourceHandler)
    {
        $this->assertResponseStatusCodeSame($resourceHandler->getExpectedStatusCode());

        /** @var IriConverter $iriConverter */
        $iriConverter = static::$container->get('api_platform.iri_converter');

        try {
            $iriConverter->getItemFromIri($resourceHandler->getExpectedIri());
        } catch (InvalidArgumentException $e) {
            $this->assertInstanceOf(ItemNotFoundException::class, $e);
        }
    }

    public function assertResourceBasicSoftDeleteSuccessTest(ResourceHandler $resourceHandler)
    {
        $this->assertResourceBasicDeleteSuccessTest($resourceHandler);

        static::$container->get('doctrine.orm.default_entity_manager')
            ->getFilters()
            ->getFilter('softdeleteable')
            ->disableForEntity($this->getResourceName());

        /** @var IriConverter $iriConverter */
        $iriConverter = static::$container->get('api_platform.iri_converter');

        $this->assertResponseStatusCodeSame($resourceHandler->getExpectedStatusCode());

        $this->assertIsObject($iriConverter->getItemFromIri($resourceHandler->getExpectedIri()));
    }

    public function assertResourceBasicValidationTest(ResourceHandler $resourceHandler, array $fields)
    {
        $response = $resourceHandler->getResponse();
        $responseArray = $response->getArray();

        $this->assertResponseStatusCodeSame(422);
        $this->assertJsonContains([
            '@context' => '/contexts/ConstraintViolationList',
            '@type' => 'ConstraintViolationList',
        ]);
        $this->assertArrayHasKey('violations', $responseArray);

        $violationKey = 0;
        foreach ($fields as $key => $value) {
            if (is_integer($key)) {
                $propertyPath = $value;
                $message = null;
            } else {
                $propertyPath = $key;
                $message = $value;
            }

            $this->assertEquals($propertyPath, $responseArray['violations'][$violationKey]['propertyPath'] ?? null);

            if ($message) {
                $this->assertEquals($message, $responseArray['violations'][$violationKey]['message'] ?? null);
            }

            ++$violationKey;
        }
    }

    public function assertResourceBasicNoAuthTest()
    {
        $this->assertResourceUnauthorized('JWT Token not found');
    }

    public function assertResourceUnauthorized(string $message = null)
    {
        $json['code'] = 401;
        $message ? $json['message'] = $message : null;

        $this->assertResponseStatusCodeSame(401);

        $this->assertJsonContains($json);
    }

    public function assertResourceBasicAccessDenied()
    {
        $this->assertResponseStatusCodeSame(403);
        $this->assertJsonContains([
            '@context' => '/contexts/Error',
            '@type' => 'hydra:Error',
            'hydra:title' => 'An error occurred',
            'hydra:description' => 'Access Denied.',
        ]);
    }

    /**
     * @throws \ReflectionException
     */
    protected function getResourceShortName(): string
    {
        return (new \ReflectionClass($this->getResourceName()))->getShortName();
    }

    abstract protected function getResourceName(): string;
}
