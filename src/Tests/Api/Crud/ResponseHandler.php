<?php

namespace App\Tests\Api\Crud;

use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ResponseHandler
{
    private ResponseInterface $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function get(): ResponseInterface
    {
        return $this->response;
    }

    public function getArray(): array
    {
        try {
            return $this->response->toArray(false);
        } catch (ExceptionInterface $e) {
            return [];
        }
    }
}
