<?php

namespace App\Tests\Api\Crud;

use Symfony\Contracts\HttpClient\ResponseInterface;

class ResourceHandler
{
    private string $url;
    private ?array $resourceFields;
    private ?int $resourceResponseStatusCode;
    private ResponseHandler $responseHandler;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function setResourceFields(array $fields): self
    {
        $this->resourceFields = $fields;

        return $this;
    }

    public function addResourceFields(array $fields): self
    {
        $this->resourceFields = array_merge($this->resourceFields, $fields);

        return $this;
    }

    public function setResourceResponseStatusCode(int $code): self
    {
        $this->resourceResponseStatusCode = $code;

        return $this;
    }

    public function createResponseHandler(ResponseInterface $response): self
    {
        $this->responseHandler = new ResponseHandler($response);

        return $this;
    }

    public function getResponse(): ResponseHandler
    {
        return $this->responseHandler;
    }

    public function getExpectedIri(): string
    {
        return $this->url;
    }

    public function getExpectedStatusCode(): ?int
    {
        return $this->resourceResponseStatusCode;
    }

    /**
     * @return array|string[]
     */
    public function getExpectedResponseFields(string $resourceName, string ...$fields): array
    {
        $array = [
            '@context' => "/contexts/$resourceName",
            '@type' => $resourceName,
        ];

        foreach ($fields as $field) {
            $array[$field] = $this->resourceFields[$field];
        }

        return $array;
    }

    /**
     * @return string
     */
    public function getExpectedContentType()
    {
        return 'application/ld+json; charset=utf-8';
    }
}
