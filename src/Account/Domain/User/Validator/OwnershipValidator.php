<?php

namespace App\Account\Domain\User\Validator;

use App\Account\Domain\User\UserOwnershipEntityInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintValidator;

class OwnershipValidator extends ConstraintValidator
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param UserOwnershipEntityInterface $value
     * @param Constraint&Ownership $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var UserOwnershipEntityInterface $contextEntity */
        $contextEntity = $this->context->getObject();
        if (!$contextEntity instanceof UserOwnershipEntityInterface) {
            throw new \InvalidArgumentException('The "OwnershipValidator" is intended for "UserOwnershipEntityInterface" only.');
        }

        $contextUser = $contextEntity->getUser() ?? $this->security->getUser();

        /** use {@see NotBlank} instead */
        if (null === $value || '' === $value) {
            return;
        }

        if ($contextUser === $value->getUser()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value->getUser()->getUsername())
            ->setParameter('{{ owner }}', $contextUser->getUsername())
            ->addViolation();
    }
}
