<?php

namespace App\Account\Domain\User\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Ownership extends Constraint
{
    public string $message = 'This resource belongs to the "{{ owner }}". The "{{ value }}" passed.';
}
