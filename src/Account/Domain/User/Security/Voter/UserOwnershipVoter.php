<?php

namespace App\Account\Domain\User\Security\Voter;

use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use App\Creator\Domain\Deck\Model\Deck;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserOwnershipVoter extends Voter
{
    const GET_PERMISSION = 'GET';
    const POST_PERMISSION = 'POST';
    const PUT_PERMISSION = 'PUT';
    const PATCH_PERMISSION = 'PATCH';
    const DELETE_PERMISSION = 'DELETE';

    const PERMISSIONS = [
        self::GET_PERMISSION,
        self::POST_PERMISSION,
        self::PUT_PERMISSION,
        self::PATCH_PERMISSION,
        self::DELETE_PERMISSION,
    ];

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, self::PERMISSIONS)
            && is_subclass_of($subject, UserOwnershipEntityInterface::class);
    }

    /**
     * @param string $attribute
     * @param Deck $subject
     *
     * @return bool
     *
     * @throws \Exception
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if (self::POST_PERMISSION === $attribute) {
            return $this->voteOnPost();
        }

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::GET_PERMISSION:
                return $this->voteOnGet($subject, $user);
            case self::PUT_PERMISSION:
                return $this->voteOnPut($subject, $user);
            case self::PATCH_PERMISSION:
                return $this->voteOnPatch($subject, $user);
            case self::DELETE_PERMISSION:
                return $this->voteOnDelete($subject, $user);
        }

        throw new \Exception(sprintf('Unhandled attribute "%s"', $attribute));
    }

    private function voteOnGet(Deck $subject, UserInterface $user): bool
    {
        if ($subject->getUser() === $user) {
            return true;
        }

        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        return false;
    }

    private function voteOnPost(): bool
    {
        return true;
    }

    private function voteOnPut(Deck $subject, UserInterface $user): bool
    {
        return $this->voteOnGet($subject, $user);
    }

    private function voteOnPatch(Deck $subject, UserInterface $user): bool
    {
        return $this->voteOnGet($subject, $user);
    }

    private function voteOnDelete(Deck $subject, UserInterface $user): bool
    {
        return $this->voteOnGet($subject, $user);
    }
}
