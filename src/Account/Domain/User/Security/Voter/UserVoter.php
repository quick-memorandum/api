<?php

namespace App\Account\Domain\User\Security\Voter;

use App\Account\Domain\User\Model\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    const PUT_PERMISSION = 'PUT';
    const PATCH_PERMISSION = 'PATCH';
    const DELETE_PERMISSION = 'DELETE';

    const PERMISSIONS = [
        self::PUT_PERMISSION,
        self::PATCH_PERMISSION,
        self::DELETE_PERMISSION,
    ];

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, self::PERMISSIONS)
            && $subject instanceof User;
    }

    /**
     * @param string $attribute
     * @param User $subject
     *
     * @return bool
     *
     * @throws \Exception
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case self::PUT_PERMISSION:
                return $this->voteOnPut($subject, $user);
            case self::PATCH_PERMISSION:
                return $this->voteOnPatch($subject, $user);
            case self::DELETE_PERMISSION:
                return $this->voteOnDelete($subject, $user);
        }

        throw new \Exception(sprintf('Unhandled attribute "%s"', $attribute));
    }

    private function voteOnPut(User $subject, UserInterface $user): bool
    {
        if ($subject === $user) {
            return true;
        }

        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        return false;
    }

    private function voteOnPatch(User $subject, UserInterface $user): bool
    {
        return $this->voteOnPut($subject, $user);
    }

    private function voteOnDelete(User $subject, UserInterface $user): bool
    {
        return $this->voteOnPut($subject, $user);
    }
}
