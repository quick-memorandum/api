<?php

namespace App\Account\Domain\User;

use App\Account\Domain\User\Model\User;

interface UserOwnershipEntityInterface
{
    public const AUTO_WRITE_GROUP = 'auto:write:user';

    public function getUser(): ?User;

    public function setUser(User $user): self;
}
