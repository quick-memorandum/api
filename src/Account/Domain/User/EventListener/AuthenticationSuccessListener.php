<?php

namespace App\Account\Domain\User\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Symfony\Component\HttpFoundation\Cookie;

class AuthenticationSuccessListener
{
    /**
     * Auto-enables "secure" flag when the request is already using HTTPS.
     *
     * @see Cookie::$secure
     */
    private const COOKIE_SECURE = null;

    private int $tokenTtl;

    public function __construct(int $tokenTtl)
    {
        $this->tokenTtl = $tokenTtl;
    }

    /**
     * @return JWTAuthenticationSuccessResponse
     *
     * @throws \Exception
     */
    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        /** @var JWTAuthenticationSuccessResponse $response */
        $response = $event->getResponse();
        $data = $event->getData();

        $token = $data['token'];

        $response->headers->setCookie(
            new Cookie(
                'BEARER',
                $token,
                (new \DateTime())->add(new \DateInterval('PT'.$this->tokenTtl.'S')),
                '/',
                null,
                self::COOKIE_SECURE
            )
        );

        return $response;
    }
}
