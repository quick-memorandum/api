<?php

namespace App\Account\Domain\User\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;

class RefreshTokenListener implements EventSubscriberInterface
{
    /**
     * Auto-enables "secure" flag when the request is already using HTTPS.
     *
     * @see Cookie::$secure
     */
    private const COOKIE_SECURE = null;

    private int $ttl;

    public function __construct(int $ttl)
    {
        $this->ttl = $ttl;
    }

    /**
     * @throws \Exception
     */
    public function setRefreshToken(AuthenticationSuccessEvent $event)
    {
        $refreshToken = $event->getData()['refresh_token'];
        $response = $event->getResponse();

        if ($refreshToken) {
            $response->headers->setCookie(
                new Cookie(
                    'REFRESH_TOKEN',
                    $refreshToken,
                    (new \DateTime())->add(new \DateInterval('PT'.$this->ttl.'S')),
                    '/',
                    null,
                    self::COOKIE_SECURE
                )
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_authentication_success' => [
                ['setRefreshToken'],
            ],
        ];
    }
}
