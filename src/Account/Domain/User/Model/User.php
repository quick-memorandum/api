<?php

namespace App\Account\Domain\User\Model;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Account\Domain\User\UserDefinition;
use App\Shared\Application\ApiDefinition\Definition;
use App\Shared\UI\Filter\PropertyDefinitionFilter;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      attributes={
 *          "security"="is_granted('ROLE_USER')",
 *          "normalization_context"={"groups"={User::READ_GROUP}},
 *          "denormalization_context"={"groups"={User::CREATE_GROUP, User::UPDATE_GROUP}}
 *      },
 *      collectionOperations={
 *          "get",
 *          "post"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *              "denormalization_context"={"groups"={User::CREATE_GROUP}}
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "delete"={
 *              "security"="is_granted('DELETE', object)"
 *          },
 *          "put"={
 *              "security"="is_granted('PUT', object)",
 *              "denormalization_context"={"groups"={User::UPDATE_GROUP}}
 *          },
 *          "patch"={
 *              "security"="is_granted('PATCH', object)",
 *              "denormalization_context"={"groups"={User::UPDATE_GROUP}}
 *          }
 *      }
 * )
 * @ApiFilter(PropertyDefinitionFilter::class, arguments={
 *     "entityDefinition": UserDefinition::class
 * })
 * @ORM\Entity(repositoryClass="App\Account\Infrastructure\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @Gedmo\SoftDeleteable(hardDelete=false)
 */
#[Definition([UserDefinition::class])]
class User implements UserInterface
{
    use SoftDeleteableEntity;

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    const READ_GROUP = 'user:read';
    const READ_ADDITIONAL_GROUP = 'user:read:additional';
    const READ_ADMIN_GROUP = 'admin:read';
    const CREATE_GROUP = 'user:create';
    const UPDATE_GROUP = 'user:update';
    const WRITE_ADMIN_GROUP = 'admin:write';

    /**
     * @var string|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid", unique=true)
     *
     * @Groups({User::READ_GROUP})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max="180")
     * @Assert\Email()
     *
     * @Groups({
     *     User::READ_GROUP,
     *     User::CREATE_GROUP,
     *     User::UPDATE_GROUP
     * })
     *
     * TODO: Visible for admin and owner only
     */
    private $email;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     *
     * @Groups({
     *     User::READ_ADMIN_GROUP,
     *     User::WRITE_ADMIN_GROUP
     * })
     */
    private $roles = [];

    /**
     * @var string The hashed password
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string|null
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="8", max="255")
     *
     * @Groups({
     *     User::CREATE_GROUP,
     *     User::UPDATE_GROUP
     * })
     * @SerializedName("password")
     */
    private $plainPassword;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @Groups({User::READ_GROUP})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     * @Groups({User::READ_GROUP})
     */
    private $updatedAt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     *
     * @Groups({User::READ_GROUP})
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantee every user at least has ROLE_USER
        $roles[] = self::ROLE_USER;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Clear sensitive and temporary data.
     *
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
