<?php

namespace App\Account\Domain\User;

use App\Account\Domain\User\Model\User;
use App\Shared\Application\ApiDefinition\AbstractEntityDefinition;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class UserDefinition extends AbstractEntityDefinition
{
    const DEFAULT_PROPERTIES = [
        'id',
        'email',
        'username',
        'roles',
        'createdAt',
        'updatedAt',
    ];

    public function getDefaultNormalizationContext(): array
    {
        return [
            AbstractNormalizer::ATTRIBUTES => self::DEFAULT_PROPERTIES,
        ];
    }

    public function getTargetEntity(): string
    {
        return User::class;
    }

    public function getDefaultPropertyGroup(): string
    {
        return User::READ_GROUP;
    }

    public function getAdditionalPropertyGroup(): string
    {
        return User::READ_ADDITIONAL_GROUP;
    }
}
