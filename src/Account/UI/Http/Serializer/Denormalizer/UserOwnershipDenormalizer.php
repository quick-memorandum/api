<?php

namespace App\Account\UI\Http\Serializer\Denormalizer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

/**
 * The main idea behind this denormalizer is to automatically set an authenticated user
 * to entities implementing {@see UserOwnershipEntityInterface}.
 *
 * @note Cache for this denormalizer is turned off {@see self::hasCacheableSupportsMethod}, so this class has to be as fast as possible.
 */
final class UserOwnershipDenormalizer implements ContextAwareDenormalizerInterface, CacheableSupportsMethodInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    private Security $security;
    private IriConverterInterface $iriConverter;

    private const ALREADY_CALLED = 'OWNERSHIP_DENORMALIZER_ALREADY_CALLED';

    public function __construct(Security $security, IriConverterInterface $iriConverter)
    {
        $this->security = $security;
        $this->iriConverter = $iriConverter;
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return array_key_exists(UserOwnershipEntityInterface::class, class_implements($type));
    }

    /**
     * @return UserOwnershipEntityInterface
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        // Avoid issues with proxies if we populated the object
        if (isset($data['@id']) && !isset($context[AbstractNormalizer::OBJECT_TO_POPULATE])) {
            if (true !== ($context['api_allow_update'] ?? true)) {
                throw new NotNormalizableValueException('Update is not allowed for this operation.');
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $this->iriConverter->getItemFromIri($data['@id'], $context + ['fetch_data' => true]);
        }

        if (
            !isset($context[AbstractNormalizer::OBJECT_TO_POPULATE])
            && (
                !$this->security->isGranted(User::ROLE_ADMIN)
                || ($this->security->isGranted(User::ROLE_ADMIN) && !isset($data['user']))
            )
        ) {
            $context[AbstractNormalizer::GROUPS][] = UserOwnershipEntityInterface::AUTO_WRITE_GROUP;
            $data['user'] = $this->iriConverter->getIriFromItem($this->security->getUser());
        }

        // to denormalize 'user' first
        if (isset($data['user'])) {
            $user = $data['user'];
            unset($data['user']);

            $data = array_merge(
                ['user' => $user],
                $data
            );
        }

        return $this->denormalizer->denormalize($data, $type, $format, $context);
    }

    /**
     * The cache is not supported because we depend on ALREADY_CALLED const.
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return false;
    }
}
