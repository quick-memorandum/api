<?php

namespace App\Shared\Application\Query;

interface IdentifiableQueryInterface
{
    public function setIdentifier(string $uuid): void;
}
