<?php

namespace App\Shared\Application\Service;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter;

class SoftDeletable
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function disableFilter($class)
    {
        /** @var SoftDeleteableFilter $softDeletableFilter */
        $softDeletableFilter = $this->entityManager->getFilters()->getFilter('softdeleteable');
        $softDeletableFilter->disableForEntity($class);
    }
}
