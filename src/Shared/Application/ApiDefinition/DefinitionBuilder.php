<?php

namespace App\Shared\Application\ApiDefinition;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Annotation\Groups;

class DefinitionBuilder
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getPropertiesDefinition(array $properties, AbstractEntityDefinition $entityDefinition): array
    {
        $return = [];

        foreach ($properties as $property) {
            try {
                $reflectionProperty = new \ReflectionProperty($entityDefinition->getTargetEntity(), $property);
                $attributes = $reflectionProperty->getAttributes(Definition::class);

                /** @var Definition $propertyAnnotation */
                $propertyAnnotation = array_pop($attributes)->newInstance();
            } catch (\ReflectionException $e) {
                continue;
            }

            $return[$property] = $propertyAnnotation->getEntityDefinition();
        }

        return $return;
    }

    public function fillProperties(array $properties, AbstractEntityDefinition $definition, array $groups = null): array
    {
        $returnProperties = [];
        $returnGroups = $groups ?? [];
        $returnGroups[] = $definition->getDefaultPropertyGroup();
        $returnGroups[] = $definition->getAdditionalPropertyGroup();

        $scalarProps = $this->getProperties($definition, ['scalar'], $returnGroups);
        /** @var AbstractEntityDefinition[] $definitionProps */
        $definitionProps = $this->getPropertiesDefinition($this->getProperties($definition, ['item', 'collection'], $returnGroups), $definition);

        foreach ($properties as $k => $v) {
            if (\is_integer($k) && \is_string($v)) {
                if (\in_array($v, $scalarProps)) {
                    $returnProperties[] = $v;
                } elseif (\in_array($v, \array_keys($definitionProps))) {
                    $returnGroups[] = $definitionProps[$v]->getDefaultPropertyGroup();
                    $returnGroups[] = $definitionProps[$v]->getAdditionalPropertyGroup();
                    list($returnProperties[$v], $returnGroups) = $this->nestedDefinitionsToEmptyProps($definitionProps[$v], $returnGroups);
                }
            } elseif (\is_string($k) && \is_array($v)) {
                if (\in_array($k, \array_keys($definitionProps))) {
                    $entityDefinition = $definitionProps[$k];

                    if (empty($v)) {
                        $returnProperties[$k] = [];
                        $returnGroups = \array_unique(\array_merge($returnGroups, [$entityDefinition->getDefaultPropertyGroup()]));
                    } else {
                        $fill = $this->fillProperties($v, $entityDefinition, $returnGroups);
                        $returnProperties[$k] = $fill['properties'];
                        $returnGroups = \array_unique(\array_merge($returnGroups, $fill['groups']));
                    }
                }
            }
        }

        // TODO: Some abstraction can help to get rid of this ugly array return.
        return [
            'properties' => $returnProperties,
            'groups' => $returnGroups,
        ];
    }

    private function nestedDefinitionsToEmptyProps(AbstractEntityDefinition $definition, array $groups = null): array
    {
        $propertiesDefinition = $this->getPropertiesDefinition($this->getProperties($definition, ['item']), $definition);
        $groups ??= [];

        /** @var AbstractEntityDefinition $def */
        foreach ($propertiesDefinition as $prop => $def) {
            $groups[] = $def->getDefaultPropertyGroup();
        }

        return [
            $this->getProperties($definition, ['scalar'], $groups) + \array_map(fn () => [], $propertiesDefinition),
            $groups,
        ];
    }

    private function getProperties(AbstractEntityDefinition $definition, array $types, array $groups = null): array
    {
        $annotationReader = new AnnotationReader();
        $reflectionExtractor = new ReflectionExtractor();
        $doctrineExtractor = new DoctrineExtractor($this->entityManager);
        $allProperties = $reflectionExtractor->getProperties($definition->getTargetEntity());

        $return = [];

        foreach ($allProperties as $property) {
            $propertyGroups = null;

            try {
                $reflection = new \ReflectionProperty($definition->getTargetEntity(), $property);
                /** @var Groups|null $propertyGroups */
                $propertyGroups = $annotationReader->getPropertyAnnotation(
                    $reflection,
                    Groups::class
                );
            } catch (\ReflectionException $e) {
            }

            if (null === $propertyGroups) {
                foreach (ReflectionExtractor::$defaultAccessorPrefixes as $accessorPrefix) {
                    try {
                        $reflection = new \ReflectionMethod($definition->getTargetEntity(), $accessorPrefix.$this->camelize($property));
                        /** @var Groups|null $propertyGroups */
                        $propertyGroups = $annotationReader->getMethodAnnotation(
                            $reflection,
                            Groups::class
                        );

                        if ($propertyGroups) {
                            break;
                        }
                    } catch (\ReflectionException $e) {
                    }
                }
            }

            if (!isset($reflection)) {
                continue;
            }

            if (null !== $propertyGroups) {
                $propertyType = $doctrineExtractor->getTypes($definition->getTargetEntity(), $property)[0] ??
                    $reflectionExtractor->getTypes($definition->getTargetEntity(), $property)[0];

                if (null !== $groups && empty(array_intersect($groups, $propertyGroups->getGroups()))) {
                    continue;
                }

                $isCollection = $isItem = false;

                if (!empty($reflection->getAttributes(Definition::class)) && null !== $propertyType->getClassName()) {
                    $isCollection = $propertyType->isCollection();
                    $isItem = !$isCollection;
                }

                if ($isCollection && \in_array('collection', $types)) {
                    $return[] = $property;
                } elseif ($isItem && \in_array('item', $types)) {
                    $return[] = $property;
                } elseif (!$isCollection && !$isItem && \in_array('scalar', $types)) {
                    $return[] = $property;
                }
            }
        }

        return $return;
    }

    private function camelize(string $string): string
    {
        return \str_replace(' ', '', \ucwords(\str_replace('_', ' ', $string)));
    }
}
