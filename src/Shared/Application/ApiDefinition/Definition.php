<?php

namespace App\Shared\Application\ApiDefinition;

use Attribute;
use Doctrine\Common\Annotations\AnnotationException;

#[Attribute(Attribute::TARGET_PROPERTY | Attribute::TARGET_CLASS)]
final class Definition
{
    private AbstractEntityDefinition $entityDefinition;

    /**
     * Definition constructor.
     *
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    public function __construct(array $options)
    {
        $definition = $options[0] ?? null;

        if (null !== $definition) {
            $reflectionClass = new \ReflectionClass($definition);

            if (
                !$reflectionClass->isAbstract() &&
                $reflectionClass->isSubclassOf(AbstractEntityDefinition::class)
            ) {
                $this->entityDefinition = new $definition();
            } else {
                throw AnnotationException::creationError('@Definition requires value to be an object that extends AbstractEntityDefinition');
            }
        } else {
            throw AnnotationException::creationError('@Definition requires object value that extends AbstractEntityDefinition');
        }
    }

    public function getEntityDefinition(): AbstractEntityDefinition
    {
        return $this->entityDefinition;
    }
}
