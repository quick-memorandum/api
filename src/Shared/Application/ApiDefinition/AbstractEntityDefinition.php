<?php

namespace App\Shared\Application\ApiDefinition;

abstract class AbstractEntityDefinition
{
    abstract public function getDefaultNormalizationContext(): array;

    abstract public function getTargetEntity(): string;

    abstract public function getDefaultPropertyGroup(): string;

    abstract public function getAdditionalPropertyGroup(): string;
}
