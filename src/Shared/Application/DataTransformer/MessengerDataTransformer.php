<?php

namespace App\Shared\Application\DataTransformer;

use ApiPlatform\Core\Bridge\Symfony\Messenger\DataTransformer;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Account\Domain\User\Model\User;
use App\Account\Domain\User\UserOwnershipEntityInterface;
use Symfony\Component\Security\Core\Security;

final class MessengerDataTransformer implements DataTransformerInterface
{
    private DataTransformer $decorated;
    private Security $security;

    public function __construct(DataTransformer $decorated, Security $security)
    {
        $this->decorated = $decorated;
        $this->security = $security;
    }

    public function transform($object, string $to, array $context = [])
    {
        if ($object instanceof UserOwnershipEntityInterface && null === $object->getUser()) {
            /** @var User $authUser */
            $authUser = $this->security->getUser();

            $object->setUser($authUser);
        }

        return $object;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $this->decorated->supportsTransformation($data, $to, $context);
    }
}
