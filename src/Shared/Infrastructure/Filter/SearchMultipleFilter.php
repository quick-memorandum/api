<?php

namespace App\Shared\Infrastructure\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class SearchMultipleFilter extends AbstractContextAwareFilter
{
    private string $parameterName;

    public function __construct(
        ManagerRegistry $managerRegistry,
        ?RequestStack $requestStack = null,
        LoggerInterface $logger = null,
        array $properties = null,
        NameConverterInterface $nameConverter = null,
        string $parameterName = null,
    ) {
        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);

        if (null === $properties) {
            throw new \InvalidArgumentException('The "properties" has to be specified.');
        }

        if (null === $parameterName) {
            throw new \InvalidArgumentException('The "parameterName" has to be specified.');
        }

        $this->parameterName = $parameterName;
    }

    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if (null === $value || $property !== $this->parameterName) {
            return;
        }

        $conditions = [];

        foreach (array_keys($this->properties) as $prop) {
            $conditions[] = sprintf('ILIKE(o.%s,:value) = TRUE', $prop);
        }

        $queryBuilder->andWhere($queryBuilder->expr()->orX(...$conditions));
        $queryBuilder->setParameter('value', "%$value%");
    }

    public function getDescription(string $resourceClass): array
    {
        $description = sprintf('Filter by following fields: %s.', implode(', ', $this->properties));

        return [
            $this->parameterName => [
                'property' => $this->parameterName,
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'description' => $description,
                    'name' => "{$this->parameterName}",
                    'type' => 'string',
                ],
                'openapi' => [
                    'description' => $description,
                    'name' => "{$this->parameterName}",
                    'schema' => [
                        'type' => 'string',
                    ],
                ],
            ],
        ];
    }
}
