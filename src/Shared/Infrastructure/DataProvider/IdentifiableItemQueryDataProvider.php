<?php

namespace App\Shared\Infrastructure\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use App\Shared\Application\Query\IdentifiableQueryInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class IdentifiableItemQueryDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    private const QUERY_ATTRIBUTE = 'query';

    private ?IdentifiableQueryInterface $queryMessage = null;

    public function __construct(
        private ResourceMetadataFactoryInterface $resourceMetadataFactory,
        private MessageBusInterface $queryBus
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return null !== $this->getIdentifiableQueryMessage($resourceClass, $operationName);
    }

    /**
     * @throws \Throwable
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        $this->queryMessage->setIdentifier($id);

        try {
            $envelope = $this->queryBus->dispatch($this->queryMessage);
        } catch (HandlerFailedException $e) {
            throw $e->getPrevious();
        }

        /** @var HandledStamp $handled */
        $handled = $envelope->last(HandledStamp::class);

        return $handled->getResult();
    }

    private function getIdentifiableQueryMessage(string $resourceClass, string $operationName): ?IdentifiableQueryInterface
    {
        if (null !== $this->queryMessage) {
            return $this->queryMessage;
        }

        $queryClassName = $this->resourceMetadataFactory
            ->create($resourceClass)
            ->getItemOperationAttribute($operationName, self::QUERY_ATTRIBUTE);

        if (null === $queryClassName) {
            return null;
        }

        $queryMessage = new $queryClassName();

        if ($queryMessage instanceof IdentifiableQueryInterface) {
            $this->queryMessage = $queryMessage;

            return $queryMessage;
        }

        return null;
    }
}
