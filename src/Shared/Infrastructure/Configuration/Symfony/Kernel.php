<?php

namespace App\Shared\Infrastructure\Configuration\Symfony;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private const CONFIG_RELATIVE_PATH = '../../../../../config';
    private const APP_RELATIVE_PATH = '../../../..';

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import(self::CONFIG_RELATIVE_PATH.'/{packages}/*.yaml');
        $container->import(self::CONFIG_RELATIVE_PATH.'/{packages}/'.$this->environment.'/*.yaml');
        $container->import(self::APP_RELATIVE_PATH.'/*/Infrastructure/Configuration/Symfony/Package/*.yaml');
        $container->import(self::APP_RELATIVE_PATH.'/*/Infrastructure/Configuration/Symfony/Package/'.$this->environment.'/*.yaml');

        if (is_file(\dirname(__DIR__).'/../../../../config/services.yaml')) {
            $container->import(self::CONFIG_RELATIVE_PATH.'/services.yaml');
            $container->import(self::CONFIG_RELATIVE_PATH.'/{services}_'.$this->environment.'.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/../../../../config/services.php')) {
            (require $path)($container->withPath($path), $this);
        }
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import(self::CONFIG_RELATIVE_PATH.'/{routes}/'.$this->environment.'/*.yaml');
        $routes->import(self::CONFIG_RELATIVE_PATH.'/{routes}/*.yaml');

        if (is_file(\dirname(__DIR__).'/../../../../config/routes.yaml')) {
            $routes->import(self::CONFIG_RELATIVE_PATH.'/routes.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/../../../../config/routes.php')) {
            (require $path)($routes->withPath($path), $this);
        }
    }
}
