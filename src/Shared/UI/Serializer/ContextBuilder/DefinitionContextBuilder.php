<?php

namespace App\Shared\UI\Serializer\ContextBuilder;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Shared\Application\ApiDefinition\Definition;
use App\Shared\Application\ApiDefinition\DefinitionBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @todo Try to use ContextAwareNormalizerInterface instead
 * @experimental
 */
final class DefinitionContextBuilder implements SerializerContextBuilderInterface
{
    private SerializerContextBuilderInterface $decorated;
    private DefinitionBuilder $definitionBuilder;

    public function __construct(
        SerializerContextBuilderInterface $decorated,
        DefinitionBuilder $definitionBuilder
    ) {
        $this->decorated = $decorated;
        $this->definitionBuilder = $definitionBuilder;
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);

        if (!$normalization) {
            return $context;
        }

        if ($context['output']) {
            unset($context[AbstractNormalizer::ATTRIBUTES]);

            return $context;
        }

        $resourceClass = $context['resource_class'] ?? null;
        $resourceClassReflection = new \ReflectionClass($resourceClass);
        $attributes = $resourceClassReflection->getAttributes(Definition::class);

        /** @var Definition $definitionClassAnnotation */
        $definitionClassAnnotation = array_pop($attributes)->newInstance();

        $definition = $definitionClassAnnotation->getEntityDefinition();

        $this->replaceContextValues($context, $definition->getDefaultNormalizationContext());

        $fill = $this->definitionBuilder->fillProperties($context[AbstractNormalizer::ATTRIBUTES], $definition, $context[AbstractNormalizer::GROUPS]);
        $context[AbstractNormalizer::ATTRIBUTES] = $fill['properties'];
        $context[AbstractNormalizer::GROUPS] = \array_values(\array_unique(\array_merge($context[AbstractNormalizer::GROUPS], $fill['groups'])));

        return $context;
    }

    /**
     * @note Method supports array values only
     */
    private function replaceContextValues(array &$context, array $extraContext): void
    {
        foreach ($extraContext as $item => $value) {
            $context[$item] = \array_replace($context[$item] ?? [], $value);
        }
    }
}
