<?php

namespace App\Shared\UI\OpenApi;

use App\Shared\UI\OpenApi\Path\PathInterface;

class PathManager extends AbstractManager
{
    /** @var PathInterface[] */
    private array $paths;

    public function setPath(PathInterface $path): self
    {
        $this->paths[] = $path;

        return $this;
    }

    public function create(): void
    {
        foreach ($this->paths as $path) {
            $this->openApi->getPaths()->addPath($path->getName(), $path->getItem());
        }
    }
}
