<?php

namespace App\Shared\UI\OpenApi;

use App\Shared\UI\OpenApi\Schema\SchemaInterface;

class SchemaManager extends AbstractManager
{
    /** @var SchemaInterface[] */
    private array $schemas;

    public function setSchema(SchemaInterface $schema): self
    {
        $this->schemas[] = $schema;

        return $this;
    }

    public function create(): void
    {
        $allSchemas = $this->openApi->getComponents()->getSchemas();

        foreach ($this->schemas as $schema) {
            $allSchemas->offsetSet($schema->getName(), $schema->getSchema());
        }
    }
}
