<?php

namespace App\Shared\UI\OpenApi\Path;

use ApiPlatform\Core\OpenApi\Model;

interface PathInterface
{
    public function getName(): string;

    public function getItem(): Model\PathItem;
}
