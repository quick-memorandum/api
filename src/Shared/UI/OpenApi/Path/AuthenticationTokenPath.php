<?php

namespace App\Shared\UI\OpenApi\Path;

use ApiPlatform\Core\OpenApi\Model;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationTokenPath implements PathInterface
{
    public function getName(): string
    {
        return '/authentication_token';
    }

    public function getItem(): Model\PathItem
    {
        return (new Model\PathItem())->withPost(
            (new Model\Operation(
                'postAuthenticationToken',
                ['Token'],
                [
                    Response::HTTP_OK => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                ],
                'Get JWT token to login.',
                '',
                null,
                [],
                new Model\RequestBody(
                    'Create new JWT token.',
                    new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                    true
                ),
                null,
                false,
                [],
            ))
        );
    }
}
