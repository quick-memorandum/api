<?php

namespace App\Shared\UI\OpenApi\Path;

use ApiPlatform\Core\OpenApi\Model;
use Symfony\Component\HttpFoundation\Response;

class RefreshTokenPath implements PathInterface
{
    public function getName(): string
    {
        return '/refresh_token';
    }

    public function getItem(): Model\PathItem
    {
        return (new Model\PathItem())->withPost(
            (new Model\Operation(
                'postRefreshToken',
                ['Token'],
                [
                    Response::HTTP_OK => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                ],
                'Refresh the JWT token.',
                '',
                null,
                [],
                new Model\RequestBody(
                    'Refresh the JWT token.',
                    new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Refresh',
                            ],
                        ],
                    ]),
                    true
                ),
                null,
                false,
                [],
            ))
        );
    }
}
