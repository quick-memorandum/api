<?php

namespace App\Shared\UI\OpenApi\Schema;

class TokenSchema implements SchemaInterface
{
    public function getName(): string
    {
        return 'Token';
    }

    public function getSchema(): array
    {
        return [
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'refresh_token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ];
    }
}
