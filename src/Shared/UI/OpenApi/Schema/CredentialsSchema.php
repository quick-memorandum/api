<?php

namespace App\Shared\UI\OpenApi\Schema;

class CredentialsSchema implements SchemaInterface
{
    public function getName(): string
    {
        return 'Credentials';
    }

    public function getSchema(): array
    {
        return [
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'user@example.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'password',
                ],
            ],
        ];
    }
}
