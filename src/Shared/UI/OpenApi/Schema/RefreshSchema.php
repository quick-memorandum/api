<?php

namespace App\Shared\UI\OpenApi\Schema;

class RefreshSchema implements SchemaInterface
{
    public function getName(): string
    {
        return 'Refresh';
    }

    public function getSchema(): array
    {
        return [
            'type' => 'object',
            'properties' => [
                'refresh_token' => [
                    'type' => 'string',
                ],
            ],
        ];
    }
}
