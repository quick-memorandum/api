<?php

namespace App\Shared\UI\OpenApi\Schema;

interface SchemaInterface
{
    public function getName(): string;

    public function getSchema(): array;
}
