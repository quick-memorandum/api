<?php

namespace App\Shared\UI\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model;
use ApiPlatform\Core\OpenApi\OpenApi;
use App\Shared\UI\OpenApi\Path\AuthenticationTokenPath;
use App\Shared\UI\OpenApi\Path\RefreshTokenPath;
use App\Shared\UI\OpenApi\Schema\CredentialsSchema;
use App\Shared\UI\OpenApi\Schema\RefreshSchema;
use App\Shared\UI\OpenApi\Schema\TokenSchema;

class OpenApiFactory implements OpenApiFactoryInterface
{
    private const HTTP_METHODS = ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS', 'HEAD', 'PATCH', 'TRACE'];

    private OpenApiFactoryInterface $decorated;

    public function __construct(OpenApiFactoryInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        (new SchemaManager($openApi))
            ->setSchema(new TokenSchema())
            ->setSchema(new RefreshSchema())
            ->setSchema(new CredentialsSchema())
            ->create();

        (new PathManager($openApi))
            ->setPath(new AuthenticationTokenPath())
            ->setPath(new RefreshTokenPath())
            ->create();

        $this->addUnauthorizedResponseCode($openApi);

        return $openApi;
    }

    private function addUnauthorizedResponseCode(OpenApi $openApi)
    {
        /** @var Model\PathItem $pathItem */
        foreach ($openApi->getPaths()->getPaths() as $pathItem) {
            foreach (self::HTTP_METHODS as $method) {
                $methodName = sprintf('get%s', ucfirst(strtolower($method)));

                if (null !== $pathItem->$methodName()) {
                    $pathItem->$methodName()->addResponse(new Model\Response('Unauthorized'), 401);
                }
            }
        }
    }
}
