<?php

namespace App\Shared\UI\OpenApi;

use ApiPlatform\Core\OpenApi\OpenApi;

abstract class AbstractManager
{
    protected OpenApi $openApi;

    public function __construct(OpenApi $openApi)
    {
        $this->openApi = $openApi;
    }

    abstract public function create(): void;
}
