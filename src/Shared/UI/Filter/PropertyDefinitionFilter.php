<?php

namespace App\Shared\UI\Filter;

use ApiPlatform\Core\Serializer\Filter\FilterInterface;
use App\Shared\Application\ApiDefinition\AbstractEntityDefinition;
use App\Shared\Application\ApiDefinition\DefinitionBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @experimental
 */
final class PropertyDefinitionFilter implements FilterInterface
{
    const REPLACE_PROPERTIES_PARAMETER = 'replaceProperties';
    const APPEND_PROPERTIES_PARAMETER = 'appendProperties';

    private AbstractEntityDefinition $entityDefinition;
    private ?DefinitionBuilder $definitionBuilder;
    private ?NameConverterInterface $nameConverter;

    public function __construct(
        string $entityDefinition = null,
        ?DefinitionBuilder $definitionBuilder = null,
        ?NameConverterInterface $nameConverter = null
    ) {
        $this->entityDefinition = new $entityDefinition();
        $this->definitionBuilder = $definitionBuilder;
        $this->nameConverter = $nameConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, bool $normalization, array $attributes, array &$context)
    {
        if (!$normalization) {
            return;
        }

        if (\array_key_exists(self::REPLACE_PROPERTIES_PARAMETER, $commonAttribute = $request->attributes->get('_api_filters', []))) {
            $parameterName = self::REPLACE_PROPERTIES_PARAMETER;
            $properties = $commonAttribute[self::REPLACE_PROPERTIES_PARAMETER];
        } elseif (\array_key_exists(self::APPEND_PROPERTIES_PARAMETER, $commonAttribute = $request->attributes->get('_api_filters', []))) {
            $parameterName = self::APPEND_PROPERTIES_PARAMETER;
            $properties = $commonAttribute[self::APPEND_PROPERTIES_PARAMETER];
        } elseif (!empty($request->query->all(self::REPLACE_PROPERTIES_PARAMETER))) {
            $parameterName = self::REPLACE_PROPERTIES_PARAMETER;
            $properties = $request->query->all(self::REPLACE_PROPERTIES_PARAMETER);
        } elseif (!empty($request->query->all(self::APPEND_PROPERTIES_PARAMETER))) {
            $parameterName = self::APPEND_PROPERTIES_PARAMETER;
            $properties = $request->query->all(self::APPEND_PROPERTIES_PARAMETER);
        } else {
            return;
        }

        $properties = \array_filter($properties);
        $groups = [];

        if (empty($properties)) {
            return;
        }

        $properties = $this->denormalizeProperties($properties);
        $context[AbstractNormalizer::ATTRIBUTES] ??= [];
        $context[AbstractNormalizer::GROUPS] ??= [];

        if (self::APPEND_PROPERTIES_PARAMETER === $parameterName) {
            $fill = $this->definitionBuilder->fillProperties($properties, $this->entityDefinition, $context[AbstractNormalizer::GROUPS]);
            $properties = \array_merge(
                $context[AbstractNormalizer::ATTRIBUTES],
                $fill['properties']
            );
            $groups = \array_merge($groups, $fill['groups']);
        } else {
            $fill = $this->definitionBuilder->fillProperties($properties, $this->entityDefinition, $context[AbstractNormalizer::GROUPS]);
            $properties = $fill['properties'];
            $groups = \array_merge($groups, $fill['groups']);
        }

        $context[AbstractNormalizer::ATTRIBUTES] = $properties;
        $context[AbstractNormalizer::GROUPS] = $groups;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return $this->getParameterDescription(true) +
            $this->getParameterDescription(false);
    }

    private function denormalizeProperties(array $properties): array
    {
        if (null === $this->nameConverter || !$properties) {
            return $properties;
        }

        $result = [];
        foreach ($properties as $key => $value) {
            $result[$this->denormalizePropertyName((string) $key)] = \is_array($value) ? $this->denormalizeProperties($value) : $this->denormalizePropertyName($value);
        }

        return $result;
    }

    private function denormalizePropertyName($property)
    {
        return null !== $this->nameConverter ? $this->nameConverter->denormalize($property) : $property;
    }

    private function getParameterDescription(bool $append): array
    {
        if ($append) {
            $parameter = self::APPEND_PROPERTIES_PARAMETER;
        } else {
            $parameter = self::REPLACE_PROPERTIES_PARAMETER;
        }

        $description = 'Example usage: '.
            \sprintf(
                '%1$s[]={propertyName}&%1$s[]={anotherPropertyName}&%1$s[{nestedPropertyParent}][]={nestedProperty}',
                $parameter
            );

        return [
            "{$parameter}[]" => [
                'property' => null,
                'type' => 'string',
                'is_collection' => true,
                'required' => false,
                'swagger' => [
                    'description' => $description,
                    'name' => "{$parameter}[]",
                    'type' => 'array',
                    'items' => [
                        'type' => 'string',
                    ],
                ],
                'openapi' => [
                    'description' => $description,
                    'name' => "{$parameter}[]",
                    'schema' => [
                        'type' => 'array',
                        'items' => [
                            'type' => 'string',
                        ],
                    ],
                ],
            ],
        ];
    }
}
